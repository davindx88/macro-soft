//
//  App.swift
//  Macro SOFT
//
//  Created by Davin Djayadi on 12/10/21.
//

import Foundation

class App {
    static let shared = App()
    
    private init(){}
    
    var propertyService: PropertyService {
        return DefaultPropertyService()
    }
   
    var listNamaRoom = DefaultListNamaRoomService()

    var virtualTourService: VirtualTourService = DefaultVirtualTourService()
    var infoOnboarding: OnBoardingService = InfoOnBoardingDefaultService()
   
}

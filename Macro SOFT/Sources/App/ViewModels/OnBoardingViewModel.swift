//
//  OnBoardingViewModel.swift
//  Macro SOFT
//
//  Created by rubby handojo on 09/11/21.
//

import UIKit
struct OnBoardingViewModel {
    let titleOnboarding: String
    let subTitleOnboarding: String
    let imageOnboarding:UIImage
    
    init (onboarding: InfoOnBoarding){
        titleOnboarding = onboarding.title
        subTitleOnboarding = onboarding.subTitle
        imageOnboarding = onboarding.image
    }
}

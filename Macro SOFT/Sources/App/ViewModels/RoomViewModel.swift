//
//  RoomViewModel.swift
//  Macro SOFT
//
//  Created by rubby handojo on 18/10/21.
//

import Foundation
import UIKit
struct RoomViewModel{
    let nama: String
    let img :UIImage
    
    init (room: VirtualTourRoom){
        img = room.uiImage!
        nama = room.name
    }
}

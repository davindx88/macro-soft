//
//  PropertyViewModel.swift
//  Macro SOFT
//
//  Created by Davin Djayadi on 12/10/21.
//

import Foundation
import UIKit

class PropertyViewModel{
    let imageStrURL: String
    let name: String
    let address: String
    let propertyAreaStr: String
    let nBathroomStr: String
    let nBedroomStr: String
    let priceStr: String
    let description: String
    
    var image: UIImage? = nil
    
    init(property: Property){
        imageStrURL = property.image
        name = property.name
        address = property.address
        propertyAreaStr = "\(property.width * property.height) m2"
        nBathroomStr = "\(property.totalBathroom) baths"
        nBedroomStr = "\(property.totalBedroom) rooms"
        priceStr = "Rp. \(property.price / 1_000_000_000) Milyar"
        description = property.description
    }
    
    init(virtualTour: VirtualTour){
        imageStrURL = virtualTour.rooms[0].image
        name = virtualTour.name
        address = virtualTour.address
        propertyAreaStr = "\(virtualTour.houseArea) m2"
        nBathroomStr = "\(virtualTour.numberOfBath) baths"
        nBedroomStr = "\(virtualTour.numberOfBed) rooms"
        priceStr = "Rp. \(virtualTour.price / 1_000_000_000) Milyar"
        description = virtualTour.description
        
        // Load Image
    }
    
    func loadImage(onSuccess: @escaping(UIImage)->Void, onFailed: @escaping(String)->Void){
        if let img = self.image {
            onSuccess(img)
        }else{
            UIImage.loadFromURL(
                urlString: imageStrURL,
                onSuccess: { [weak self] image in
                    self?.image = image
                    onSuccess(image)
                },
                onFailed: { reason in
                    onFailed(reason)
                })
        }
    }
}

//
//  DescriptionViewModel.swift
//  Macro SOFT
//
//  Created by rubby handojo on 21/10/21.
//

import Foundation
import UIKit
struct DescriptionViewModel{
    let namaRumah: String
    let lokasiRumah: String
    let jumlahKamarMandi: String
    let jumlahKamarTidur: String
    let luasRumah: String
    let hargaRumah: String
    let description: String
    let numberFormatter = NumberFormatter()
    
}

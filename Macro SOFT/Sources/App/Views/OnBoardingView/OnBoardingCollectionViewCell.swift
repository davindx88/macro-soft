//
//  OnBoardingCollectionViewCell.swift
//  Macro SOFT
//
//  Created by rubby handojo on 09/11/21.
//

import UIKit

class OnBoardingCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var image: UIImageView!
    var presenter: OnBoardingPresenter?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    var OnboardingModel:OnBoardingViewModel!{
        didSet{
            title.text = OnboardingModel.titleOnboarding
            subTitle.text = OnboardingModel.subTitleOnboarding
            image.image = OnboardingModel.imageOnboarding
        }
    }
//    func setup(_ slide:OnBoardingViewModel){
//        print("masuk setup")
//        image.image = slide.image
//        title.text = slide.title
//        subTitle.text = slide.subTitle
//    }

}

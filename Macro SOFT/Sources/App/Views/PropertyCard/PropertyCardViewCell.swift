//
//  PropertyCardViewCell.swift
//  Macro SOFT
//
//  Created by Davin Djayadi on 07/10/21.
//

import UIKit

class PropertyCardViewCell: UITableViewCell {
    @IBOutlet var propImgView: UIImageView!
    @IBOutlet var propNameLabel: UILabel!
    @IBOutlet var propAddrLabel: UILabel!
    @IBOutlet var houseSizeLabel: UILabel!
    @IBOutlet var nBathLabel: UILabel!
    @IBOutlet var nBedroomLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    
    var viewModel: PropertyViewModel! {
        didSet{
            viewModel.loadImage(
                onSuccess: { [weak self] image in
                    self?.propImgView.image = image
                },
                onFailed: { _ in
                    // do nothing
                })
            propNameLabel.text = viewModel.name
            propAddrLabel.text = viewModel.address
            houseSizeLabel.text = viewModel.propertyAreaStr
            nBathLabel.text = viewModel.nBathroomStr
            nBedroomLabel.text = viewModel.nBedroomStr
            priceLabel.text = viewModel.priceStr
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

//
//  RoomCell.swift
//  Macro SOFT
//
//  Created by rubby handojo on 18/10/21.
//

import UIKit

class RoomCell: UITableViewCell {

    @IBOutlet weak var RoomImage: UIImageView!
    @IBOutlet weak var NamaRoom: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!
    
    var presenter: ModalListPresenter?
    var roomId: String?
    
    var RoomModel:RoomViewModel!{
        didSet{
            RoomImage.image = RoomModel.img
            NamaRoom.text = RoomModel.nama
        }
    }
    @IBAction func deleteTapped(_ sender: Any) {
        print("24 Tapped")
        performAction(roomID: roomId!)
    }
    
    func performAction(roomID: String) {
        presenter?.deleteRoom(roomID: roomID)
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

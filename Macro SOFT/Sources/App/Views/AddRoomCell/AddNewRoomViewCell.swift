//
//  AddNewRoomViewCell.swift
//  Macro SOFT
//
//  Created by rubby handojo on 26/10/21.
//

import UIKit

class AddNewRoomViewCell: UITableViewCell {
    @IBOutlet weak var RoomImage: UIImageView!
    @IBOutlet weak var NamaRoom: UILabel!
    
    var RoomModel:RoomViewModel!{
        didSet{
            RoomImage.image = RoomModel.img
            NamaRoom.text = RoomModel.nama
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

//
//  AddRoomViewCell.swift
//  Macro SOFT
//
//  Created by Vincentius Phillips Zhuputra on 21/10/21.
//

import UIKit


class AddRoomViewCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var txtAddRoom: UITextField!
    var presenter: Add360Presenter?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        txtAddRoom.borderStyle = .none
        self.txtAddRoom.delegate = self
        // Initialization code
    }
    
    @IBAction func returnPressed(_ sender: Any) {
        txtAddRoom.resignFirstResponder()  //if desired
        performAction()
    }
    
    func performAction() {
        presenter!.addNamaRoom(nama: txtAddRoom.text!)
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

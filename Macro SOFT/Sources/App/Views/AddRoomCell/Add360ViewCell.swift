//
//  Add360ViewCell.swift
//  Macro SOFT
//
//  Created by Vincentius Phillips Zhuputra on 15/10/21.
//

import UIKit

class Add360ViewCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var txtNamaRoom: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

//
//  ButtonAddViewCell.swift
//  Macro SOFT
//
//  Created by rubby handojo on 27/10/21.
//

import UIKit

class ButtonAddViewCell: UITableViewCell {
    @IBOutlet weak var BtnAdd: UIButton!
    var presenter: AddRoomPresenter?

    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        // Initialization code
    }

    @IBAction func btnTaped(_ sender: Any) {
        print("touch")
        performAction()
    }
    func performAction() {
        print("touch2")
        presenter!.btnAddRoomTapped()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

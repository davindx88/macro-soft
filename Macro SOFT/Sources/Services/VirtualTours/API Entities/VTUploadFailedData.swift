//
//  File.swift
//  Macro SOFT
//
//  Created by Davin Djayadi on 02/11/21.
//

import Foundation

struct VTUploadFailedData: Codable{
    let err: String
}

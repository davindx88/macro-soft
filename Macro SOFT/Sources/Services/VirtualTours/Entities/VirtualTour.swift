//
//  VirtualTour.swift
//  Macro SOFT
//
//  Created by Davin Djayadi on 28/10/21.
//

import Foundation

struct VirtualTour : Codable {
    var _id: String = UUID().uuidString
    var name: String
    var address: String
    var description: String
    var houseArea: Int
    var fieldArea: Int
    var certificate: String
    var numberOfBed: Int
    var numberOfBath: Int
    var price: Int
    var rooms: [VirtualTourRoom]
    
    mutating func copyVirtualTourData(virtualTour: VirtualTour){
        self.name = virtualTour.name
        self.address = virtualTour.address
        self.description = virtualTour.description
        self.houseArea = virtualTour.houseArea
        self.fieldArea = virtualTour.fieldArea
        self.certificate = virtualTour.certificate
        self.numberOfBed = virtualTour.numberOfBed
        self.numberOfBath = virtualTour.numberOfBath
        self.price = virtualTour.price
    }
    
    init(
        name: String,
        address: String,
        description: String,
        houseArea: Int,
        fieldArea: Int,
        certificate: String,
        numberOfBed: Int,
        numberOfBath: Int,
        price: Int,
        rooms: [VirtualTourRoom])
    {
        self.name = name
        self.address = address
        self.description = description
        self.houseArea = houseArea
        self.fieldArea = fieldArea
        self.certificate = certificate
        self.numberOfBed = numberOfBed
        self.numberOfBath = numberOfBath
        self.price = price
        self.rooms = rooms
    }
}

//
//  VirtualTourRoomPoint.swift
//  Macro SOFT
//
//  Created by Davin Djayadi on 28/10/21.
//

import Foundation

struct VirtualTourRoomPoint : Codable {
    var roomId: String
    var x: Float
    var y: Float
    var z: Float
    private enum CodingKeys: String, CodingKey {
        case roomId, x, y, z
    }
    
    var pointId: String = UUID().uuidString
    
    init(roomId: String, x: Float, y: Float, z: Float){
        self.roomId = roomId
        self.x = x
        self.y = y
        self.z = z
    }
}

//
//  VirtualTourRoom.swift
//  Macro SOFT
//
//  Created by Davin Djayadi on 28/10/21.
//

import Foundation
import UIKit

struct VirtualTourRoom: Codable {
    var id: String
    var name: String
    var image: String
    var points: [VirtualTourRoomPoint]
    private enum CodingKeys: String, CodingKey {
        case id, name, image, points
    }
    
    var uiImage: UIImage? //dibuat create
    var isTemporary: Bool = false
    
    // Create Temporary Room
    init(name: String, uiImage: UIImage){
        self.id = UUID().uuidString
        self.name = name
        self.uiImage = uiImage
        self.points = []
        self.image = ""
        self.isTemporary = true
    }
}

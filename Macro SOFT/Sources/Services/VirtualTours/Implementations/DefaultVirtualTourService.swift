//
//  DefaultVirtualTourService.swift
//  Macro SOFT
//
//  Created by Davin Djayadi on 28/10/21.
//

import Foundation
import UIKit

class DefaultVirtualTourService {
    var virtualTours: [VirtualTour] = []
    var tempVirtualTour: VirtualTour?
    
    let host = "http://185.201.8.211:33000"
    
    func generateRequestInsertUpdateObject(route: String, method: String, virtualTour: VirtualTour) throws -> URLRequest{
        let roomsDict: [Dictionary<String, Any>] = virtualTour.rooms.map{ room in
            let pointsDict: [Dictionary<String, Any>] = room.points.map{ point in
                return [
                    "roomId": point.roomId,
                    "x": point.x,
                    "y": point.y,
                    "z": point.z
                ]
            }
            return [
                "id": room.id,
                "name": room.name,
                "image": room.uiImage?.getBase64String() ?? "",
                "points": pointsDict
            ]
        }
        
        let jsonObj:Dictionary<String, Any> = [
            "name": virtualTour.name,
            "address": virtualTour.address,
            "description": virtualTour.description,
            "houseArea": virtualTour.houseArea,
            "fieldArea": virtualTour.fieldArea,
            "certificate": virtualTour.certificate,
            "numberOfBed": virtualTour.numberOfBed,
            "numberOfBath": virtualTour.numberOfBath,
            "price": virtualTour.price,
            "rooms": roomsDict
        ]
        
        let apiURL = "\(self.host)/\(route)"
        var request: URLRequest = URLRequest(url: URL(string: apiURL)!)
        request.httpMethod = method
        request.httpBody = try JSONSerialization.data(withJSONObject: jsonObj, options: .prettyPrinted)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        return request
    }
}

extension DefaultVirtualTourService: VirtualTourService{
    func loadVirtualTours(onSuccess: @escaping ([VirtualTour]) -> Void, onFailed: @escaping (String) -> Void) {
        DispatchQueue.global(qos: .utility).async {
            let url = URL(string: "\(self.host)/api/virtual-tour")
            let dataTask = URLSession.shared.dataTask(with: url!) {data, _, _ in
                guard let jsonData = data else {
                    DispatchQueue.main.async {
                        onFailed("Failed to get data")
                    }
                    return
                }
                do {
                    let decoder = JSONDecoder()
                    let recResponse = try decoder.decode([VirtualTour].self, from: jsonData)
                    
                    self.virtualTours = recResponse
                    
                    DispatchQueue.main.async {
                        onSuccess(self.virtualTours)
                    }
                } catch {
                    DispatchQueue.main.async {
                        onFailed("Failed to decode")
                    }
                }
            }
            
            dataTask.resume()
        }
    }
    
    func uploadVirtualTour(virtualTour: VirtualTour, onSuccess: @escaping () -> Void, onFailed: @escaping (String) -> Void) {
        do{
            let request = try self.generateRequestInsertUpdateObject(route: "api/virtual-tour",
                                                                 method: "POST",
                                                                 virtualTour: virtualTour)
            NSURLConnection.sendAsynchronousRequest(request, queue: .main, completionHandler: { (response, data, error) in
                if let err = error{
                    DispatchQueue.main.async {
                        onFailed(err.localizedDescription)
                    }
                    return
                }
                guard let data = data else {
                    DispatchQueue.main.async {
                        onFailed("Data not found")
                    }
                    return
                }
                if let httpresponse = response as? HTTPURLResponse{
                    if httpresponse.statusCode == 200 {
                        DispatchQueue.main.async { onSuccess() }
                        return
                    }
                    if httpresponse.statusCode == 400 {
                        do {
                            let decoder = JSONDecoder()
                            let recResponse: VTUploadFailedData = try decoder.decode(VTUploadFailedData.self, from: data)
                            DispatchQueue.main.async { onFailed(recResponse.err) }
                        } catch {
                            DispatchQueue.main.async { onFailed("Unknown error, please try again later") }
                        }
                    }
                }
                DispatchQueue.main.async { onFailed("Unknown error, please try again later") }
            })
        }catch{
            onFailed("Error parsed data, please try again later")
        }
    }
    
    func updateVirtualTour(virtualTour: VirtualTour, onSuccess: @escaping () -> Void, onFailed: @escaping (String) -> Void) {
        do{
            let request = try self.generateRequestInsertUpdateObject(route: "api/virtual-tour/\(virtualTour._id)",
                                                                     method: "PUT",
                                                                     virtualTour: virtualTour)
            NSURLConnection.sendAsynchronousRequest(request, queue: .main, completionHandler: { (response, data, error) in
                if let err = error{
                    DispatchQueue.main.async {
                        onFailed(err.localizedDescription)
                    }
                    return
                }
                guard let data = data else {
                    DispatchQueue.main.async {
                        onFailed("Data not found")
                    }
                    return
                }
                if let httpresponse = response as? HTTPURLResponse{
                    if httpresponse.statusCode == 200 {
                        DispatchQueue.main.async { onSuccess() }
                        return
                    }
                    if httpresponse.statusCode == 400 {
                        do {
                            let decoder = JSONDecoder()
                            let recResponse: VTUploadFailedData = try decoder.decode(VTUploadFailedData.self, from: data)
                            DispatchQueue.main.async { onFailed(recResponse.err) }
                        } catch {
                            DispatchQueue.main.async { onFailed("Unknown error, please try again later") }
                        }
                    }
                }
                DispatchQueue.main.async { onFailed("Unknown error, please try again later") }
            })
        }catch{
            onFailed("Error parsed data, please try again later")
        }
    }
    
    func deleteVirtualTour(virtualTour: VirtualTour, onSuccess: @escaping () -> Void, onFailed: @escaping (String) -> Void) {
        let apiURL = "\(self.host)/api/virtual-tour/\(virtualTour._id)"
        var request: URLRequest = URLRequest(url: URL(string: apiURL)!)
        request.httpMethod = "DELETE"
        
        NSURLConnection.sendAsynchronousRequest(request, queue: .main, completionHandler: { (response, data, error) in
            if let err = error{
                DispatchQueue.main.async {
                    onFailed(err.localizedDescription)
                }
                return
            }
            guard let data = data else {
                DispatchQueue.main.async {
                    onFailed("Data not found")
                }
                return
            }
            if let httpresponse = response as? HTTPURLResponse{
                if httpresponse.statusCode == 200 {
                    DispatchQueue.main.async { onSuccess() }
                    return
                }
                if httpresponse.statusCode == 400 || httpresponse.statusCode == 404 {
                    do {
                        let decoder = JSONDecoder()
                        let recResponse: VTUploadFailedData = try decoder.decode(VTUploadFailedData.self, from: data)
                        DispatchQueue.main.async { onFailed(recResponse.err) }
                    } catch {
                        DispatchQueue.main.async { onFailed("Unknown error, please try again later") }
                    }
                }
            }
            DispatchQueue.main.async { onFailed("Unknown error, please try again later") }
        })
    }
    
    func getVirtualTours() -> [VirtualTour] {
        return self.virtualTours
    }
    
    // MARK: Buat Insert / Update
    func createTempVirtualTour(){
        let virtualTour = VirtualTour(name: "",
                                      address: "",
                                      description: "",
                                      houseArea: 0,
                                      fieldArea: 0,
                                      certificate: "",
                                      numberOfBed: 0,
                                      numberOfBath: 0,
                                      price: 0,
                                      rooms: [])
        self.tempVirtualTour = virtualTour
    }
    
    func getTempVirtualTour() -> VirtualTour {
        return tempVirtualTour!
    }
    
    func setTempVirtualTour(virtualTour: VirtualTour) {
        self.tempVirtualTour = virtualTour
    }
    
    func removeTempVirtualTour(){
        self.tempVirtualTour = nil
    }
    
    func addRoomToTempVirtualTour(room: VirtualTourRoom) {
        self.tempVirtualTour?.rooms.append(room)
    }
    
    func getRoomsOfTempVirtualTour() -> [VirtualTourRoom] {
        return self.tempVirtualTour!.rooms
    }
    
    func getRoomOfTempVirtualTour(roomId: String) -> VirtualTourRoom {
        return self.getRoomsOfTempVirtualTour().first{ $0.id == roomId }!
    }
    
    func getRoomOfTempVirtualTour(roomIdx: Int) -> VirtualTourRoom {
        return self.getRoomsOfTempVirtualTour()[roomIdx]
    }
    
    func getRoomIdxOfRoom(roomId: String) -> Int? {
        return self.tempVirtualTour?.rooms.firstIndex{ $0.id == roomId }
    }
    
    func removeRoomOfTempVirtualTour(roomId: String) {
        if let roomIdx = self.getRoomIdxOfRoom(roomId: roomId){
            self.tempVirtualTour?.rooms.remove(at: roomIdx)
        }
    }
    
    func saveRoomOfTempVirtualTour(roomId: String, fotoRuangan: UIImage) {
        if let roomIdx = self.getRoomIdxOfRoom(roomId: roomId){
            var room = self.tempVirtualTour!.rooms[roomIdx]
            room.isTemporary = false
            room.uiImage = fotoRuangan
            self.tempVirtualTour?.rooms[roomIdx] = room
        }
    }
    
    func addPointToTempVirtualTourRoom(roomId: String, point: VirtualTourRoomPoint) {
        var room = self.getRoomOfTempVirtualTour(roomId: roomId)
        room.points.append(point)
        guard let roomIdx = self.getRoomIdxOfRoom(roomId: roomId) else { return }
        self.tempVirtualTour?.rooms[roomIdx] = room
    }
    
    func removePointOfTempVirtualTourRoom(roomId: String, pointId: String) {
        guard let roomIdx = self.getRoomIdxOfRoom(roomId: roomId) else { return }
        guard let room = self.tempVirtualTour?.rooms[roomIdx] else { return }
        guard let pointIdx = room.points.firstIndex(where: { $0.pointId == pointId }) else { return }
        self.tempVirtualTour?.rooms[roomIdx].points.remove(at: pointIdx)
    }
    func loadRoomImagesOfTempVirtualTourRoom(onSuccess: @escaping () -> Void, onFailed: @escaping () -> Void) {
        guard let virtualTour = self.tempVirtualTour else { return }
        let rooms = virtualTour.rooms
        var ctr = 0
        for (idx, room) in rooms.enumerated() {
            UIImage.loadFromURL(
                urlString: room.image,
                onSuccess: { img in
                    ctr += 1
                    print(ctr)
                    self.tempVirtualTour?.rooms[idx].uiImage = img
                    if ctr == rooms.count {
                        onSuccess()
                    }
                },
                onFailed: { _ in
                    onFailed()
                })
        }
    }
}

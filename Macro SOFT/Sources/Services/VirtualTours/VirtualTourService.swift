//
//  VirtualTourService.swift
//  Macro SOFT
//
//  Created by Davin Djayadi on 28/10/21.
//

import Foundation
import UIKit

protocol VirtualTourService {
    func loadVirtualTours(onSuccess: @escaping ([VirtualTour])->Void, onFailed: @escaping (String)->Void) // buat load dari server
    func getVirtualTours() -> [VirtualTour]
    func uploadVirtualTour(virtualTour: VirtualTour, onSuccess: @escaping ()->Void, onFailed: @escaping (String)->Void) // buat upload ke server
    func updateVirtualTour(virtualTour: VirtualTour, onSuccess: @escaping ()->Void, onFailed: @escaping (String)->Void) // buat update ke server
    func deleteVirtualTour(virtualTour: VirtualTour, onSuccess: @escaping ()->Void, onFailed: @escaping (String)->Void) // buat delete ke server
    func createTempVirtualTour()
    func getTempVirtualTour() -> VirtualTour
    func setTempVirtualTour(virtualTour: VirtualTour)
    func removeTempVirtualTour()
    func addRoomToTempVirtualTour(room: VirtualTourRoom)
    func getRoomsOfTempVirtualTour() -> [VirtualTourRoom]
    func getRoomOfTempVirtualTour(roomId: String) -> VirtualTourRoom
    func getRoomOfTempVirtualTour(roomIdx: Int) -> VirtualTourRoom
    func removeRoomOfTempVirtualTour(roomId: String)
    func saveRoomOfTempVirtualTour(roomId: String, fotoRuangan: UIImage)
    func addPointToTempVirtualTourRoom(roomId: String, point: VirtualTourRoomPoint)
    func removePointOfTempVirtualTourRoom(roomId: String, pointId: String)
    func loadRoomImagesOfTempVirtualTourRoom(onSuccess: @escaping ()->Void, onFailed: @escaping ()->Void)
}

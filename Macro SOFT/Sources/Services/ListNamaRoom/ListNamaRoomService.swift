//
//  ListNamaRoomService.swift
//  Macro SOFT
//
//  Created by rubby handojo on 23/10/21.
//

import Foundation
protocol ListNamaRoomService {
    func listNamaRoom() -> [NamaRoom]
    func addNama(nama: NamaRoom)
}

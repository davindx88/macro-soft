//
//  DefaultListNamaRoomService.swift
//  Macro SOFT
//
//  Created by rubby handojo on 23/10/21.
//

import Foundation
class DefaultListNamaRoomService{
    var ListNamaRoom: [NamaRoom] = [
        NamaRoom(namaRuangan: "Ruang Tidur"),
        NamaRoom(namaRuangan: "Ruang Makan"),
        NamaRoom(namaRuangan: "Ruang Keluarga"),
        NamaRoom(namaRuangan: "Toilet/Kamar Mandi"),
        NamaRoom(namaRuangan: "Garasi"),
        NamaRoom(namaRuangan: "Teras"),
        NamaRoom(namaRuangan: "Dapur")
    ]
}

extension DefaultListNamaRoomService: ListNamaRoomService{
    
    func listNamaRoom() -> [NamaRoom] {
        return self.ListNamaRoom
    }
    
    func addNama(nama: NamaRoom){
        self.ListNamaRoom.append(nama)
        print(self.ListNamaRoom)
    }
}


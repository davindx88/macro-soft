//
//  Property.swift
//  Macro SOFT
//
//  Created by Davin Djayadi on 12/10/21.
//

import Foundation

struct Property {
    let name: String
    let address: String
    let image: String // URL
    let width: Float
    let height: Float
    let totalBathroom: Int
    let totalBedroom: Int
    let price: Int
    let description: String
}

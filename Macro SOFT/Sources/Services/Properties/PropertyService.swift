//
//  PropertyService.swift
//  Macro SOFT
//
//  Created by Davin Djayadi on 12/10/21.
//

import Foundation

protocol PropertyService {
    func properties() -> [Property]
}

//
//  PropertyDefaultService.swift
//  Macro SOFT
//
//  Created by Davin Djayadi on 12/10/21.
//

import Foundation

class DefaultPropertyService{
    var listProperty: [Property] = [
        Property(name: "House 1",
                 address: "Green St. 12-23",
                 image: "https://images.adsttc.com/media/images/5efe/1f7f/b357/6540/5400/01d7/newsletter/archdaily-houses-104.jpg?1593712501",
                 width: 10,
                 height: 4,
                 totalBathroom: 2,
                 totalBedroom: 3,
                 price: 5_000_000_000,
                 description: "Ini adalah description \n contoh description"),
        Property(name: "House 2",
                 address: "Blue St. 12-23",
                 image: "https://images.adsttc.com/media/images/5efe/1f7f/b357/6540/5400/01d7/newsletter/archdaily-houses-104.jpg?1593712501",
                 width: 4,
                 height: 5,
                 totalBathroom: 10,
                 totalBedroom: 12,
                 price: 100_000_000_000,
                 description: "ini adalah contoh description")
    ]
}

extension DefaultPropertyService: PropertyService{
    func properties() -> [Property] {
        return self.listProperty
    }
}

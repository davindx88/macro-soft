//
//  OnBoardingService.swift
//  Macro SOFT
//
//  Created by rubby handojo on 10/11/21.
//

import Foundation
import UIKit
struct InfoOnBoarding {
    let title: String
    let subTitle: String
    let image:UIImage
}

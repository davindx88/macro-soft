//
//  OnBoardingService.swift
//  Macro SOFT
//
//  Created by rubby handojo on 10/11/21.
//

import Foundation
protocol OnBoardingService{
    func isNewUser() -> Bool
    func isNotNewUser()
}

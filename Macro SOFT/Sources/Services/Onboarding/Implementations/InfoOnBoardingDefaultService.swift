//
//  InfoOnBoarding.swift
//  Macro SOFT
//
//  Created by rubby handojo on 10/11/21.
//

import Foundation
import UIKit
class InfoOnBoardingDefaultService{
    
}
extension InfoOnBoardingDefaultService:OnBoardingService{
    func isNotNewUser() {
        UserDefaults.standard.set(true, forKey: "isNewUser")
    }
    
    func isNewUser() -> Bool {
        return !UserDefaults.standard.bool(forKey: "isNewUser")
    }
    
   
    
    
}

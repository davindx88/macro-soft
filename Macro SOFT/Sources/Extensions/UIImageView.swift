//
//  UIImageView.swift
//  Macro SOFT
//
//  Created by Davin Djayadi on 14/10/21.
//

import Foundation
import UIKit

extension UIImageView {
    func load(url: URL) {
        UIImage.loadFromURL(
            urlString: url.absoluteString,
            onSuccess: { [weak self] img in
                self?.image = img
            },
            onFailed: { reason in
                print(reason)
            })
    }
    
    func load(urlString: String){
        if let url = URL(string: urlString){
            self.load(url: url)
        }else{
            print("[ImageView] Img cannot be loaded from \(urlString)")
        }
    }
}

//
//  UIView.swift
//  Macro SOFT
//
//  Created by Davin Djayadi on 07/10/21.
//

import Foundation
import UIKit

extension UIViewController{
    static func loadFromNib() -> Self {
        func instantiateFromNib<T: UIViewController>() -> T {
            return T.init(nibName: String(describing: T.self), bundle: nil)
        }
        return instantiateFromNib()
    }
}

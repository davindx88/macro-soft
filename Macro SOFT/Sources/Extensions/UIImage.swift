//
//  UIImage.swift
//  Macro SOFT
//
//  Created by Davin Djayadi on 26/10/21.
//

import Foundation
import UIKit

var dictImage: Dictionary<String,UIImage> = [:]

extension UIImage{
    static func loadFromURL(urlString: String, onSuccess: @escaping(UIImage)->Void, onFailed: @escaping(String)->Void) -> Void {
        guard let url = URL(string: urlString) else {
            onFailed("Invalid URL")
            return
        }
        
        if dictImage.keys.contains{ $0 == urlString} {
            let img: UIImage = dictImage[urlString]!
            
            onSuccess(img)
            return
        }
        
        DispatchQueue.global().async {
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    dictImage[urlString] = image
                    DispatchQueue.main.async {
                        onSuccess(image)
                    }
                }else{
                    onFailed("Invalid image data from URL")
                }
            }else{
                onFailed("Failed to load image from URL")
            }
        }
    }
    
    func getBase64String() -> String {
        let base64str: String = (self.jpegData(compressionQuality: 0.5)?.base64EncodedString(options: .lineLength64Characters))!
        return base64str
    }
    
    func image(alpha: CGFloat) -> UIImage? {
         UIGraphicsBeginImageContextWithOptions(size, false, scale)
         draw(at: .zero, blendMode: .normal, alpha: alpha)
         let newImage = UIGraphicsGetImageFromCurrentImageContext()
         UIGraphicsEndImageContext()
         return newImage
     }
}

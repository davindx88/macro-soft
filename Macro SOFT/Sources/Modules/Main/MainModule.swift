//
//  MainModule.swift
//  Macro SOFT
//
//  Created by Davin Djayadi on 06/10/21.
//

import Foundation
import UIKit

class MainModule {

    func buildDefault() -> UIViewController {
        let view = MainDefaultView()
        let interactor = MainDefaultInteractor()
        let presenter = MainDefaultPresenter()
        let router = MainDefaultRouter()

        view.presenter = presenter

        presenter.interactor = interactor
        presenter.view = view
        presenter.router = router

        interactor.presenter = presenter

        router.presenter = presenter
        router.viewController = view
        
        return view
    }
}

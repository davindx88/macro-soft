//
//  PostsDefaultBuilder.swift
//  VIPER best practices
//
//  Created by Tibor Bödecs on 2019. 03. 05..
//

import Foundation
import UIKit

class Viewer360Module {
//    func buildDefault() -> UIViewController {
//        print(14)
//        let view = Viewer360DefaultView.loadFromNib()
//        let interactor = Viewer360DefaultInteractor()
//        let presenter = Viewer360DefaultPresenter()
//        let router = Viewer360DefaultRouter()
//
//        view.presenter = presenter
//
//        presenter.interactor = interactor
//        presenter.view = view
//        presenter.router = router
//
//        interactor.presenter = presenter
//
//        router.presenter = presenter
//        router.viewController = view
//
//        return view
//    }
    
    func loadImage(roomId: String, jenisViewer: JenisViewer, onClose:@escaping ()->Void) -> UIViewController {
        print("lewat sini")
        let view = Viewer360DefaultView.loadFromNib()
        let interactor = Viewer360DefaultInteractor()
        let presenter = Viewer360DefaultPresenter(roomId: roomId, jenisViewer: jenisViewer, onClose: onClose)
        let router = Viewer360DefaultRouter()

        view.presenter = presenter

        presenter.interactor = interactor
        presenter.view = view
        presenter.router = router
        
        interactor.presenter = presenter

        router.presenter = presenter
        router.viewController = view
        
        return view
    }
    func loadImageModule(roomId: String, jenisViewer: JenisViewer, onClose:@escaping ()->Void) -> UIViewController {
        let view = Viewer360Module().loadImage(roomId: roomId, jenisViewer: jenisViewer, onClose: onClose)
        
        let navControllerView = UINavigationController()
        navControllerView.pushViewController(view, animated: false)
        navControllerView.title = "Virtual Tour"
        
        return navControllerView
    }
}

//
//  ViewController.swift
//  CTPanoramaView
//
//  Created by Cihan Tek on 12/10/2016.
//  Copyright © 2016 Home. All rights reserved.
//

import UIKit
import SceneKit

class Viewer360DefaultView: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate {

    var presenter: Viewer360Presenter?
  
    @IBOutlet var panoramaView: CTPanoramaView!
    @IBOutlet var placePointBtn: UIButton!
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var replaceImgBtn: UIButton!
    @IBOutlet weak var Aim: UIImageView!
    @IBOutlet weak var deleteBtn: UIButton!
    
    override func viewDidLoad() {
//        navigationItem.rightBarButtonItem =
        super.viewDidLoad()
        print(21)
        presenter?.viewDidLoad()
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        presenter?.dismiss()
    }
    
    @IBAction func placePointBtnTapped(_ sender: UIButton) {
        panoramaView.createPointInFrontOfCamera()
        print("Btn Tapped")
    }
    
    @IBAction func inputImageTapped(_ sender: Any) {
        let image = UIImagePickerController()
        image.delegate = self
        image.sourceType = UIImagePickerController.SourceType.photoLibrary
        image.allowsEditing = false
        self.present(image, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            panoramaView.setImage(image: image)
        }
    }
 
    func tombol(x:CGFloat, y:CGFloat, z:CGFloat){
        let idScene = SCNScene(named: "contoh")
        let material = SCNMaterial()
        material.diffuse.contents = UIColor.red

        var btn = SCNNode()
        btn.geometry?.firstMaterial = material
        btn.name = "btncoba"
        btn.position = SCNVector3(x, y, z)
//        btn.scale = SCNVector3(0.1, 0.1, 0.1)
    }
    func tutupHalaman(){
        self.navigationController?.dismiss(animated: true)
    }
    @IBAction func trashPressed(_ sender: Any) {
        print("68")
        presenter?.trashPressed()
    }
    
    func loadCylindricalImage() {
        panoramaView.image = UIImage(named: "cylindrical")
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .all
    }
}

extension Viewer360DefaultView: Viewer360View{
   
    
    func setup(){}
    
    @objc func donePressed(_ sender: Any){
        presenter?.doneIsPressed(image: panoramaView.image! )
    }
    func displayAdd(img: UIImage) {
        panoramaView.image = img
        panoramaView.panoramaType = .spherical
        self.title = "Add 360"
        let buttonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(self.donePressed))
        self.navigationItem.setRightBarButton(buttonItem, animated: false)
        //panoramaView.initRoom()
    }
    
    func displayEdit(img: UIImage) {
        panoramaView.image = img
        panoramaView.panoramaType = .spherical
        self.title = "Edit 360"
        let buttonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(self.donePressed))
        self.navigationItem.setRightBarButton(buttonItem, animated: false)
        panoramaView.initRoom()
    }
    
    func displayViewer(img: UIImage) {
        panoramaView.image = img
        panoramaView.panoramaType = .spherical
        self.title = "Viewer 360"
        deleteBtn.isHidden = true
        placePointBtn.isHidden = true
        replaceImgBtn.isHidden = true
        Aim.isHidden = true
        panoramaView.initRoom()
    }
    
    func setupOnTapAdd() {
        //setting panorama view (add)
        panoramaView.onPointClick = { roomPoint in
            self.presenter!.onPointClickAdd(roomPoint: roomPoint)
           
        }
    }
    
    func setupOnTapView() {
        //setting panorama view (view)
        panoramaView.onPointClick = { roomPoint in
            self.presenter?.onPointClickView(roomPoint: roomPoint)
        }
    }
    
    func loadRoom(room: VirtualTourRoom){
        panoramaView.setImage(image: room.uiImage!)
        panoramaView.initPoints(points: room.points)
    }
    
    func setupPoints(points: [VirtualTourRoomPoint]) {
        panoramaView.initPoints(points: points)
    }
    
    func setupPointsDelete(mode: Int) {
        panoramaView.deleteTapped(mode: mode)
        if (mode == 1) {
            placePointBtn.isEnabled = false
            Aim.isHidden = true
        } else if (mode == 2){
            placePointBtn.isEnabled = true
            Aim.isHidden = false
        }
    }
    func deletePointPanoramaView(roomPoint: RoomPoint) {
        panoramaView.deletePoint(roomPoint: roomPoint)
    }
}


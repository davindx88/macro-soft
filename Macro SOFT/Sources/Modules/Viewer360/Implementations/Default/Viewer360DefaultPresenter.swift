//
//  Viewer360DefaultPresenter.swift
//  VIPER best practices
//
//  Created by Tibor Bödecs on 2019. 03. 05..
//

import Foundation
import UIKit

enum JenisViewer{
    // ketika modallist diklik
    case edit
    // dari add 360
    case add
    // dari halaman detail
    case view
}

class Viewer360DefaultPresenter {
    var router: Viewer360Router?
    var interactor: Viewer360Interactor?
    weak var view: Viewer360View?
    
    var roomId: String
    var jenisViewer: JenisViewer
    var onClose: ()->Void
    var deleteMode = false
    
    init(roomId: String, jenisViewer: JenisViewer, onClose: @escaping()->Void = {}){
        self.roomId = roomId
        self.jenisViewer = jenisViewer
        self.onClose = onClose
    }
}

extension Viewer360DefaultPresenter: Viewer360Presenter {
    
    func viewDidLoad() {
        deleteMode = false
        let room: VirtualTourRoom = App.shared.virtualTourService.getRoomOfTempVirtualTour(roomId: roomId)
        
        if (jenisViewer == .add) {
            view?.displayAdd(img: room.uiImage!)
            view?.setupOnTapAdd()
        } else if (jenisViewer == .edit){
            view?.displayEdit(img: room.uiImage!)
            view?.setupOnTapAdd()
        } else if (jenisViewer == .view) {
            UIImage.loadFromURL(
                urlString: room.image,
                onSuccess: { image in
                    self.view?.displayViewer(img: image)
                }, onFailed: { reason in
                    print(reason)
                })
            view?.setupOnTapView()
        }
        
        view?.setup()
        self.loadPoints()
    }
    
    func loadPoints(){
        let room: VirtualTourRoom = App.shared.virtualTourService.getRoomOfTempVirtualTour(roomId: roomId)
        view?.setupPoints(points: room.points)
        print("masuk")
        print(room.points.map{ $0.roomId })
    }
    
    func dismiss(){
        let room: VirtualTourRoom = App.shared.virtualTourService.getRoomOfTempVirtualTour(roomId: roomId)
        if (room.isTemporary){
            App.shared.virtualTourService.removeRoomOfTempVirtualTour(roomId: roomId)
        }
    }
    
    func doneIsPressed(image: UIImage){
        App.shared.virtualTourService.saveRoomOfTempVirtualTour(roomId: roomId, fotoRuangan: image)
        onClose()
        print("heheh")
        view?.tutupHalaman()
    }
    
    func onPointClickAdd(roomPoint: RoomPoint) {
        if (deleteMode) {
            //let room = roomPoint.point
            let isconnectPoint: Bool = roomPoint.point == nil
            if !isconnectPoint {
                App.shared.virtualTourService.removePointOfTempVirtualTourRoom(roomId: roomId, pointId: roomPoint.point!.pointId)
                loadPoints()
                view?.setupPointsDelete(mode: 1)
            }else{
                view?.deletePointPanoramaView(roomPoint: roomPoint)
            }
        } else {
            router?.openModalAddNewRoom(roomPoint: roomPoint, roomId: roomId)
        }
        
    }
    
    func onPointClickView(roomPoint: RoomPoint) {
        let roomId: String = roomPoint.point!.roomId
        let room: VirtualTourRoom = App.shared.virtualTourService.getRoomOfTempVirtualTour(roomId: roomId)
        view?.loadRoom(room: room)
    }
    
    func trashPressed() {
        if (deleteMode) {
            //Trash di klik saat mode delete
            view?.setupPointsDelete(mode: 2)
        
            deleteMode = false
        } else {
            //Trash di klik saat mode biasa
            view?.setupPointsDelete(mode: 1)
            deleteMode = true
        }
        
    }
}

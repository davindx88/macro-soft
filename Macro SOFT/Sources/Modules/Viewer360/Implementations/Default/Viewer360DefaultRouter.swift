//
//  Viewer360DefaultRouter.swift
//  VIPER best practices
//
//  Created by Tibor Bödecs on 2019. 03. 05..
//

import Foundation
import UIKit

class Viewer360DefaultRouter {

    weak var presenter: Viewer360Presenter?
    weak var viewController: UIViewController?
}

extension Viewer360DefaultRouter: Viewer360Router {
    func openModalAddNewRoom(roomPoint: RoomPoint, roomId: String) {
        let viewController = AddRoomModule().buildDefault(roomPoint: roomPoint, parentRoomId: roomId) {
            self.presenter?.loadPoints()
        }
        self.viewController?.present(viewController, animated: true)
    }
   
}

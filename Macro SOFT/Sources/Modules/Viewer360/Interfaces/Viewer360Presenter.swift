//
//  PostsPresenter.swift
//  VIPER best practices
//
//  Created by Tibor Bödecs on 2019. 03. 05..
//

import Foundation
import UIKit

protocol Viewer360Presenter: class {
    
    var router: Viewer360Router? { get set }
    var interactor: Viewer360Interactor? { get set }
    var view: Viewer360View? { get set }
    
    func viewDidLoad()
    func dismiss()
    func doneIsPressed(image: UIImage)
    func onPointClickAdd(roomPoint: RoomPoint)
    func onPointClickView(roomPoint: RoomPoint)
    func loadPoints()
    func trashPressed()
}

//
//  PostsRouter.swift
//  VIPER best practices
//
//  Created by Tibor Bödecs on 2019. 03. 05..
//

import Foundation

protocol Viewer360Router {
    
    var presenter: Viewer360Presenter? { get set }
    
    func openModalAddNewRoom(roomPoint: RoomPoint, roomId: String)
}

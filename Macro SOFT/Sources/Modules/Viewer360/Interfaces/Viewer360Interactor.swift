//
//  PostsInteractor.swift
//  VIPER best practices
//
//  Created by Tibor Bödecs on 2019. 03. 05..
//

import Foundation

protocol Viewer360Interactor {
    
    var presenter: Viewer360Presenter? { get set }
    
 
}

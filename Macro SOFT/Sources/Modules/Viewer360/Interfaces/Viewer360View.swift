//
//  PostsView.swift
//  VIPER best practices
//
//  Created by Tibor Bödecs on 2019. 03. 05..
//

import Foundation
import UIKit

protocol Viewer360View: class {
    
    var presenter: Viewer360Presenter? { get set }

    func displayAdd(img: UIImage)
    func displayEdit(img: UIImage)
    func displayViewer(img: UIImage)
    func setupOnTapAdd()
    func setupOnTapView()
    func setup()
    func setupPoints(points: [VirtualTourRoomPoint])
    func tutupHalaman()
    func loadRoom(room: VirtualTourRoom)
    func setupPointsDelete(mode: Int)
    func deletePointPanoramaView (roomPoint:RoomPoint)
}

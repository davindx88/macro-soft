//
//  DescriptionsDefaultRouter.swift
//  VIPER best practices
//
//  Created by Tibor Bödecs on 2019. 03. 05..
//

import Foundation
import UIKit

class DescriptionDefaultRouter {

    weak var presenter: DescriptionPresenter?
    weak var viewController: UIViewController?
}

extension DescriptionDefaultRouter: DescriptionRouter {

    
}

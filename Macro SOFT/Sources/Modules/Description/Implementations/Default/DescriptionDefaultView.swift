//
//  DescriptionDefaultView.swift
//  Macro SOFT
//
//  Created by rubby handojo on 19/10/21.
//

import UIKit

class DescriptionDefaultView: UIViewController,UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource{
    var presenter: DescriptionPresenter?
    @IBOutlet weak var NamaRumah: UITextField!
    @IBOutlet weak var LokasiRumah: UITextField!
    @IBOutlet weak var JumlahKamarMandi: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var JumlahKamarTidur: UITextField!
    @IBOutlet weak var sertifTextfield: UITextField!
    @IBOutlet var txtLuasRumah: UITextField!
    @IBOutlet weak var HargaRumah: UITextField!
    @IBOutlet weak var Description: UITextView!
    @IBOutlet weak var luasBangunan: UITextField!
    var pickerData :[String] = [String]()
    var SertifikatPicker = UIPickerView()
    let doneBtn = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(dismissKeyboard))
    var amt: Int = 0
    override func viewDidLoad() {
        
//        numberFormatter.usesGroupingSeparator = true
//        numberFormatter.numberStyle = .currency
//        numberFormatter.locale = Locale(identifier: "id_ID")
//        numberFormatter.minimumFractionDigits = 0
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
        SertifikatPicker.delegate = self
        SertifikatPicker.dataSource = self
        pickerData = ["SHM - Hak Milik","SHGB - Hak Guna Bangunan","PPJP - Perjanjian Jual Beli"]
        sertifTextfield.inputView = SertifikatPicker
        
        HargaRumah.delegate = self
        HargaRumah.placeholder = updateAmount()
        Description.isEditable = true
        
        Description.layer.borderColor = UIColor.systemGray6.cgColor
        
        Description.layer.borderWidth = 1.0
        Description.layer.cornerRadius = 5.0
              
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        super.viewDidLoad()
        presenter?.viewDidLoad()
    
        // Do any additional setup after loading the view.
    }
    @objc func keyboardWillShow(notification:NSNotification) {

        guard let userInfo = notification.userInfo else { return }
        var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.scrollView.convert(keyboardFrame, from: nil)

        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height + 150
        scrollView.contentInset = contentInset
    }

    @objc func keyboardWillHide(notification:NSNotification) {

        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
       
        return 1

      
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return pickerData .count
       
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
           return pickerData[row]
       }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        sertifTextfield.text = pickerData[row]
        sertifTextfield.resignFirstResponder()
    }
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let digit = Int(string){
            amt = amt * 10 + digit
            HargaRumah.text = updateAmount()
        }
        if string == ""{
            amt = amt/10
            HargaRumah.text = updateAmount()
        }
        return false
    }
    func updateAmount() -> String?{
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.currency
        numberFormatter.locale = Locale(identifier: "id_ID")
        numberFormatter.minimumFractionDigits = 0
        let amount = Double(amt/1) + Double(amt%100)/100
        return numberFormatter.string(from: NSNumber(value: amount))
    }
    
   
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension DescriptionDefaultView:DescriptionView{
    func setup(){
        self.title = "Description"
        
        let buttonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(self.backPressed))
        let buttonItem2 = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(self.savePressed))
        self.navigationItem.setLeftBarButton(buttonItem, animated: false)
        self.navigationItem.setRightBarButton(buttonItem2, animated:false)
    }
    
    @objc func backPressed(_ sender: Any){
        self.dismiss(animated: true)
    }
    
    @objc func savePressed(_ sender: Any){
        // TODO field area textfield
        // TODO certificate textfield
//        let fieldArea: Int = 100
        
        let virtualTour = VirtualTour(name: NamaRumah.text!,
                                      address: LokasiRumah.text!,
                                      description: Description.text,
                                      houseArea: Int(txtLuasRumah.text!)!,
                                      fieldArea: Int(luasBangunan.text!)!,
                                      certificate: sertifTextfield.text!,
                                      numberOfBed: Int(JumlahKamarTidur.text!)!,
                                      numberOfBath: Int(JumlahKamarMandi.text!)!,
                                      price: amt,
                                      rooms: [])
        self.presenter?.saveVirtualTourData(virtualTour: virtualTour)
        self.dismiss(animated: true)
    }
    
    func setupFieldValue(virtualTour: VirtualTour) {
        NamaRumah.text = virtualTour.name
        LokasiRumah.text = virtualTour.address
        Description.text = virtualTour.description
        txtLuasRumah.text = "\(virtualTour.houseArea)"
        JumlahKamarTidur.text = "\(virtualTour.numberOfBed)"
        JumlahKamarMandi.text = "\(virtualTour.numberOfBath)"
        luasBangunan.text = "\(virtualTour.fieldArea)"
        sertifTextfield.text = (virtualTour.certificate)
        amt = virtualTour.price
        HargaRumah.text = self.updateAmount()
    }
}


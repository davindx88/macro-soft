//
//  DescriptionDefaultPresenter.swift
//  VIPER best practices
//
//  Created by Tibor Bödecs on 2019. 03. 05..
//

import Foundation

class DescriptionDefaultPresenter {
    
    var router: DescriptionRouter?
    var interactor: DescriptionInteractor?
    weak var view: DescriptionView?
}

extension DescriptionDefaultPresenter: DescriptionPresenter {
    func viewDidLoad() {
        let virtualTour = App.shared.virtualTourService.getTempVirtualTour()
        view?.setup()
        view?.setupFieldValue(virtualTour: virtualTour)
    }
    
    func saveVirtualTourData(virtualTour: VirtualTour) {
        // Update Virtual Tour data
        var currentVirtualTour = App.shared.virtualTourService.getTempVirtualTour()
        currentVirtualTour.copyVirtualTourData(virtualTour: virtualTour)
        App.shared.virtualTourService.setTempVirtualTour(virtualTour: currentVirtualTour)
    }
}

   


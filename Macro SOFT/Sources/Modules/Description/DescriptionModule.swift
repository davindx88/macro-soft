//
//  DescriptionsDefaultBuilder.swift
//  VIPER best practices
//
//  Created by Tibor Bödecs on 2019. 03. 05..
//

import Foundation
import UIKit

class DescriptionModule {

    func buildDefault() -> UIViewController {
        let view = DescriptionDefaultView()
        let interactor = DescriptionDefaultInteractor()
        let presenter = DescriptionDefaultPresenter()
        let router = DescriptionDefaultRouter()

        view.presenter = presenter

        presenter.interactor = interactor
        presenter.view = view
        presenter.router = router

        interactor.presenter = presenter
        
        router.presenter = presenter
        router.viewController = view
        let navController = UINavigationController()

        navController.pushViewController(view, animated: false)
        return navController
    }
}

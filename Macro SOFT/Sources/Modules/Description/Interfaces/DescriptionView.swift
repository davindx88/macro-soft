//
//  DescriptionView.swift
//  VIPER best practices
//
//  Created by Tibor Bödecs on 2019. 03. 05..
//

import Foundation

protocol DescriptionView: class {
    
    var presenter: DescriptionPresenter? { get set }

    func setup()
    func setupFieldValue(virtualTour: VirtualTour)
}

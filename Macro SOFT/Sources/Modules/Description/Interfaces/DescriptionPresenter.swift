//
//  DescriptionPresenter.swift
//  VIPER best practices
//
//  Created by Tibor Bödecs on 2019. 03. 05..
//

import Foundation

protocol DescriptionPresenter: class {
    
    var router: DescriptionRouter? { get set }
    var interactor: DescriptionInteractor? { get set }
    var view: DescriptionView? { get set }
    
    func viewDidLoad()
    func saveVirtualTourData(virtualTour: VirtualTour)
}

//
//  PostsDefaultPresenter.swift
//  VIPER best practices
//
//  Created by Tibor Bödecs on 2019. 03. 05..
//

import Foundation

class PropertiesDefaultPresenter {
    
    var router: PropertiesRouter?
    var interactor: PropertiesInteractor?
    weak var view: PropertiesView?
}

extension PropertiesDefaultPresenter: PropertiesPresenter {
    func viewDidLoad() {
        view?.setup()
        view?.displayLoading()
        App.shared.virtualTourService.loadVirtualTours(
            onSuccess: { [weak self] virtualTours in
                print("[LOAD VT] Load Success")
                if virtualTours.isEmpty {
                    self?.view?.displayNoProperty()
                }else{
                    self?.view?.displayProperties(virtualTours: virtualTours)
                }
            },
            onFailed: { [weak self] reason in
                print("[LOAD VT] Load gagal karena: \(reason)")
                self?.view?.displayNoProperty()
            })
    }
    
    func propertyDidSelect(virtualTour: VirtualTour) {
        print("Pindah halaman...")
        App.shared.virtualTourService.setTempVirtualTour(virtualTour: virtualTour)
        router?.showDetailProperty(virtualTour: virtualTour)
    }
    
    func btnAddVirtualTourTapped() {
        print("Pindah ke Virtual Tour Page...")
        App.shared.virtualTourService.createTempVirtualTour()
        router?.showAddVirtualTourPage()
    }
    
    func deleteVtour(virtualTour: VirtualTour) {
        App.shared.virtualTourService.deleteVirtualTour(virtualTour: virtualTour, onSuccess: {
            
        }, onFailed: {_ in
            
        })
        viewDidLoad()
    }
}

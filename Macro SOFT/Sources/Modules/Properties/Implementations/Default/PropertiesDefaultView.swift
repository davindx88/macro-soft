//
//  PropertieDefaultView.swift
//  Macro SOFT
//
//  Created by Davin Djayadi on 07/10/21.
//

import UIKit

class PropertiesDefaultView: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var presenter: PropertiesPresenter?
    @IBOutlet var tableView: UITableView!
    @IBOutlet var rmhKosongImageView: UIImageView!
    @IBOutlet var rmhKosongLabel1: UILabel!
    @IBOutlet var rmhKosongLabel2: UILabel!
    
    var virtualTours: [VirtualTour] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.presenter?.viewDidLoad()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return virtualTours.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Property", for: indexPath) as! PropertyCardViewCell
        let virtualTour = virtualTours[indexPath.section]
        let viewModel = PropertyViewModel(virtualTour: virtualTour)
        cell.viewModel = viewModel
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let virtualTour = virtualTours[indexPath.section]
        self.presenter?.propertyDidSelect(virtualTour: virtualTour)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    func tableView(_ tableView: UITableView,
                    trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
     {
         let modifyAction = UIContextualAction(style: .normal, title:  "Delete", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
             //presenter yang manggil delete vtour terus update
             self.presenter?.deleteVtour(virtualTour: self.virtualTours[indexPath.section])
             success(true)
         })
         modifyAction.image = UIImage(named: "delete")
         modifyAction.backgroundColor = .red
     
         return UISwipeActionsConfiguration(actions: [modifyAction])
     }
    
    @IBAction func btnAddVirtualTourTapped(_ sender: UIButton) {
        presenter?.btnAddVirtualTourTapped()
    }
}

extension PropertiesDefaultView: PropertiesView {
    func setup(){
        self.title = "Virtual Tour"
    }
    
    func displayLoading() {
        rmhKosongImageView.isHidden = true
        rmhKosongLabel1.isHidden = true
        rmhKosongLabel2.isHidden = true
        print("Loading...")
    }
    
    func displayNoProperty() {
        print("No property found!")
        rmhKosongImageView.isHidden = false
        rmhKosongLabel1.isHidden = false
        rmhKosongLabel2.isHidden = false
    }
    
    func displayProperties(virtualTours: [VirtualTour]) {
        self.virtualTours = virtualTours
        tableView.showsVerticalScrollIndicator = false
        tableView.register(UINib(nibName: "PropertyCardViewCell", bundle: nil), forCellReuseIdentifier: "Property")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.reloadData()
    }
}

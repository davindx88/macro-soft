//
//  PostsDefaultRouter.swift
//  VIPER best practices
//
//  Created by Tibor Bödecs on 2019. 03. 05..
//

import Foundation
import UIKit

class PropertiesDefaultRouter {

    weak var presenter: PropertiesPresenter?
    weak var viewController: UIViewController?
}

extension PropertiesDefaultRouter: PropertiesRouter {
    func showDetailProperty(virtualTour: VirtualTour){
        let viewController = PropertyDetailModule().buildDefault(virtualTour: virtualTour){
            self.presenter?.viewDidLoad()
        }
        self.viewController?.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func showAddVirtualTourPage() {
        let viewController = ModalListModule().buildDefault{ [weak self] in
            self?.presenter?.viewDidLoad()
        }
        viewController.modalPresentationStyle = .fullScreen
        self.viewController?.navigationController?.pushViewController(viewController, animated: true)
    }
}

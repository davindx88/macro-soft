//
//  PostsDefaultBuilder.swift
//  VIPER best practices
//
//  Created by Tibor Bödecs on 2019. 03. 05..
//

import Foundation
import UIKit

class PropertiesModule {

    func buildDefault() -> UIViewController {
        let view = PropertiesDefaultView.loadFromNib()
        let interactor = PropertiesDefaultInteractor()
        let presenter = PropertiesDefaultPresenter()
        let router = PropertiesDefaultRouter()

        view.presenter = presenter

        presenter.interactor = interactor
        presenter.view = view
        presenter.router = router

        interactor.presenter = presenter

        router.presenter = presenter
        router.viewController = view
        
        let navControllerView = UINavigationController()
        navControllerView.pushViewController(view, animated: false)
        navControllerView.title = "Virtual Tour"
        
        return navControllerView
    }
}

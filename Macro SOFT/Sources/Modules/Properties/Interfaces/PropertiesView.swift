//
//  PostsView.swift
//  VIPER best practices
//
//  Created by Tibor Bödecs on 2019. 03. 05..
//

import Foundation

protocol PropertiesView: class {
    
    var presenter: PropertiesPresenter? { get set }
    
    func setup()
    func displayLoading()
    func displayNoProperty()
    func displayProperties(virtualTours: [VirtualTour])
}

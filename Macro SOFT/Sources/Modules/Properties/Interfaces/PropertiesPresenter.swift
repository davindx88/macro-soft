//
//  PostsPresenter.swift
//  VIPER best practices
//
//  Created by Tibor Bödecs on 2019. 03. 05..
//

import Foundation

protocol PropertiesPresenter: class {
    
    var router: PropertiesRouter? { get set }
    var interactor: PropertiesInteractor? { get set }
    var view: PropertiesView? { get set }
    
    func viewDidLoad()
    func propertyDidSelect(virtualTour: VirtualTour)
    func btnAddVirtualTourTapped()
    func deleteVtour(virtualTour: VirtualTour)
}

//
//  PostsRouter.swift
//  VIPER best practices
//
//  Created by Tibor Bödecs on 2019. 03. 05..
//

import Foundation

protocol PropertiesRouter {
    
    var presenter: PropertiesPresenter? { get set }
    
    func showAddVirtualTourPage()
    func showDetailProperty(virtualTour: VirtualTour)
}

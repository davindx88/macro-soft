//
//  PostsInteractor.swift
//  VIPER best practices
//
//  Created by Tibor Bödecs on 2019. 03. 05..
//

import Foundation

protocol PropertiesInteractor {
    
    var presenter: PropertiesPresenter? { get set }
}

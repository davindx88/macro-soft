//
//  ModalListModule.swift
//  Macro SOFT
//
//  Created by Vincentius Phillips Zhuputra on 12/10/21.
//

import Foundation
import UIKit

class ModalListModule {
    func buildDefault(onSuccessUploadVT: @escaping()->Void) -> UIViewController {
        let view = ModalListDefaultView()
        let interactor = ModalListDefaultInteractor()
        let presenter = ModalListDefaultPresenter(mode: .insert, onSuccessUploadVT: onSuccessUploadVT)
        let router = ModalListDefaultRouter()

        view.presenter = presenter

        presenter.interactor = interactor
        presenter.view = view
        presenter.router = router

        interactor.presenter = presenter

        router.presenter = presenter
        router.viewController = view
        
        return view
    }
    
    func buildUpdate(onSuccessUploadVT: @escaping()->Void) -> UIViewController {
        let view = ModalListDefaultView()
        let interactor = ModalListDefaultInteractor()
        let presenter = ModalListDefaultPresenter(mode:.update, onSuccessUploadVT: onSuccessUploadVT)
        let router = ModalListDefaultRouter()

        view.presenter = presenter

        presenter.interactor = interactor
        presenter.view = view
        presenter.router = router

        interactor.presenter = presenter

        router.presenter = presenter
        router.viewController = view
        
        return view
    }
}

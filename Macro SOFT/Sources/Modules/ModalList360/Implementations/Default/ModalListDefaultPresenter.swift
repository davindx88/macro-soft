//
//  ModalListDefaultPresenter.swift
//  Macro SOFT
//
//  Created by Vincentius Phillips Zhuputra on 12/10/21.
//

import Foundation
import UIKit

enum ModalListMode{
    case insert
    case update
}

class ModalListDefaultPresenter {
    
    var router: ModalListRouter?
    var interactor: ModalListInteractor?
    weak var view: ModalListView?
    
    var onSuccessUploadVT: ()->Void
    var mode: ModalListMode
    init(mode: ModalListMode, onSuccessUploadVT: @escaping()->Void){
        self.mode = mode
        self.onSuccessUploadVT = onSuccessUploadVT
    }
}

extension ModalListDefaultPresenter: ModalListPresenter {
    func viewDidLoad() {
        view?.setup()
        let rooms: [VirtualTourRoom] = App.shared.virtualTourService.getTempVirtualTour().rooms
        view?.displayRooms(rooms: rooms)
    }
    
    func btnAddRoomTapped() {
        print("Pindah ke Add Room")
        router?.showAddRoomPage()
    }
    func descriptionTapped(){
        router?.showDescription()
    }
    func roomTapped(index: Int){
        let room = App.shared.virtualTourService.getRoomOfTempVirtualTour(roomIdx: index)
        let roomIdx = room.id
        router?.openViewer(roomId: roomIdx)
    }
    func reload(){
        let rooms: [VirtualTourRoom] = App.shared.virtualTourService.getTempVirtualTour().rooms
        view?.updateRooms(rooms: rooms)
    }
    func onSubmitVirtualTour(){
        var virtualTour: VirtualTour = App.shared.virtualTourService.getTempVirtualTour()
        
        view?.displayUploadLoading()
        
        if mode == .insert {
            App.shared.virtualTourService.uploadVirtualTour(
                virtualTour: virtualTour,
                onSuccess: { [weak self] in
                    print("[Upload-VT] Successfully upload virtual tour")
                    self?.view?.displayUploadSuccess()
                },
                onFailed: { [weak self] reason in
                    print("[Upload-VT] Failed upload virtual tour karena: \(reason)")
                    self?.view?.displayUploadFailed(reason: reason)
                })
        }
        if mode == .update {
            App.shared.virtualTourService.updateVirtualTour(
                virtualTour: virtualTour,
                onSuccess: { [weak self] in
                    print("[Update-VT] Successfully update virtual tour")
                    self?.view?.displayUploadSuccess()
                },
                onFailed: { [weak self] reason in
                    print("[Update-VT] Failed update virtual tour karena: \(reason)")
                    self?.view?.displayUploadFailed(reason: reason)
                })
        }
    }
    
    func deleteRoom(roomID: String) {
        App.shared.virtualTourService.removeRoomOfTempVirtualTour(roomId: roomID)
        reload()
    }
    
    func onVirtualTourSubmmited() {
        router?.backToMainMenu()
        onSuccessUploadVT()
    }
}

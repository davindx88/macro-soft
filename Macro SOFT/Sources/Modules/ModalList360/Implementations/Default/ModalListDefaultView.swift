//
//  ModalListDefaultView.swift
//  Macro SOFT
//
//  Created by Vincentius Phillips Zhuputra on 14/10/21.
//

import UIKit

class ModalListDefaultView: UIViewController,UITableViewDelegate, UITableViewDataSource{
    var presenter: ModalListPresenter?
    var rooms: [VirtualTourRoom] = []
    var ac: UIAlertController!
    
    @IBOutlet weak var addRoomBtn: UIButton!
    @IBOutlet weak var addDescBtn: UIButton!
    @IBOutlet weak var roomTabbleView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter?.viewDidLoad()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return rooms.count
    }
    
    @IBAction func descriptionTapped(_ sender: Any) {
        presenter?.descriptionTapped()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Room",for: indexPath) as! RoomCell
        
        cell.presenter = self.presenter
        let room = rooms[indexPath.section]
        let viewModel = RoomViewModel(room: room)
        cell.roomId = room.id
        cell.RoomModel = viewModel
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let room = rooms[indexPath.section]
        let image = room.uiImage
        presenter?.roomTapped(index: indexPath.section)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    @IBAction func addRoomBtn(_ sender: Any) {
        presenter?.btnAddRoomTapped()
    }
    
    @objc func onSubmitVirtualTour(){
        presenter?.onSubmitVirtualTour()
    }
}

extension ModalListDefaultView: ModalListView {
    func setup(){
        self.title = "Add Virtual Tour"
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(onSubmitVirtualTour))
    }
    
    func displayRooms(rooms: [VirtualTourRoom]){
        roomTabbleView.showsVerticalScrollIndicator = false
        roomTabbleView.register(UINib(nibName: "RoomCell", bundle: nil), forCellReuseIdentifier: "Room")
        roomTabbleView.dataSource = self
        roomTabbleView.delegate = self
        self.updateRooms(rooms: rooms)
    }
    
    func updateRooms(rooms: [VirtualTourRoom]){
        self.rooms = rooms
        roomTabbleView.reloadData()
    }
    
    func displayUploadLoading() {
        ac = UIAlertController(title: "Uploading Virtual Tour", message: "Please kindly wait until proccess completed", preferredStyle: .alert)
        
        self.present(ac, animated: true)
    }
    
    func displayUploadSuccess() {
        ac.dismiss(animated: true){ [weak self] in
            let newAc = UIAlertController(title: "Success", message: "Virtual Tour successfully uploaded!", preferredStyle: .alert)
            newAc.addAction(UIAlertAction(title: "Ok", style: .default){ [weak self] _ in
                self?.presenter?.onVirtualTourSubmmited()
            })
            self?.present(newAc, animated: true)
        }
    }
    
    func displayUploadFailed(reason: String) {
        ac.dismiss(animated: true){ [weak self] in
            let newAc = UIAlertController(title: "Failed", message: reason, preferredStyle: .alert)
            newAc.addAction(UIAlertAction(title: "Ok", style: .default))
            self?.present(newAc, animated: true)
        }
    }
}

//
//  ModalListRouter.swift
//  Macro SOFT
//
//  Created by Vincentius Phillips Zhuputra on 12/10/21.
//

import Foundation
import UIKit

class ModalListDefaultRouter {
    weak var presenter: ModalListPresenter?
    weak var viewController: UIViewController?
}

extension ModalListDefaultRouter: ModalListRouter {
    func showAddRoomPage() {
        let viewController = Add360Module().buildDefault1(openFrom: .fromModalList){
            self.presenter?.reload()
        }
        self.viewController?.present(viewController, animated: true)
    }
    
    func showDescription() {
        let viewController = DescriptionModule().buildDefault()
        self.viewController?.present(viewController, animated: true)
        //self.viewController?.navigationController?.pushViewController(viewController, animated: true)
    }
    func openViewer(roomId: String){
        let viewController = Viewer360Module().loadImageModule(roomId: roomId, jenisViewer: .edit){
            self.presenter?.reload()
        }
        self.viewController?.present(viewController, animated: true)
    }
    func backToMainMenu() {
        self.viewController?.navigationController?.popViewController(animated: true)
    }
}


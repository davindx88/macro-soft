//
//  ModalListRouter.swift
//  Macro SOFT
//
//  Created by Vincentius Phillips Zhuputra on 12/10/21.
//

import Foundation
import UIKit
protocol ModalListRouter {
    
    var presenter: ModalListPresenter? { get set }
    
    func showAddRoomPage()
    func showDescription()
    func openViewer(roomId: String)
    func backToMainMenu()
}

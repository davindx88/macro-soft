//
//  ModalListPresenter.swift
//  Macro SOFT
//
//  Created by Vincentius Phillips Zhuputra on 12/10/21.
//

import Foundation
import UIKit
protocol ModalListPresenter: class {
    
    var router: ModalListRouter? { get set }
    var interactor: ModalListInteractor? { get set }
    var view: ModalListView? { get set }
    
    func viewDidLoad()
    func btnAddRoomTapped()
    func descriptionTapped()
    func roomTapped(index: Int)
    func reload()
    func onSubmitVirtualTour()
    func onVirtualTourSubmmited()
    func deleteRoom(roomID: String)
    
}

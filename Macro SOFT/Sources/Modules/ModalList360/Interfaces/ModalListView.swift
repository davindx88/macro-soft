//
//  ModalListView.swift
//  Macro SOFT
//
//  Created by Vincentius Phillips Zhuputra on 12/10/21.
//

import Foundation

protocol ModalListView: class {
    
    var presenter: ModalListPresenter? { get set }

    func setup()
    func displayRooms(rooms: [VirtualTourRoom])
    func updateRooms(rooms: [VirtualTourRoom])
    
    func displayUploadLoading()
    func displayUploadSuccess()
    func displayUploadFailed(reason: String)
}

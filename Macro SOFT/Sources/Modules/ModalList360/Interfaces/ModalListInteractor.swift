//
//  ModalListInteractor.swift
//  Macro SOFT
//
//  Created by Vincentius Phillips Zhuputra on 12/10/21.
//

import Foundation

protocol ModalListInteractor {
    
    var presenter: ModalListPresenter? { get set }
    
}

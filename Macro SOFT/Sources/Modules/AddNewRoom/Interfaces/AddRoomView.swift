//
//  AddRoomsView.swift
//  VIPER best practices
//
//  Created by Tibor Bödecs on 2019. 03. 05..
//

import Foundation

protocol AddRoomView: class {
    
    var presenter: AddRoomPresenter? { get set }
    func isCreateNamaRow(idxRow: Int) -> Bool
   
    func setup()
    func displayRooms(rooms: [VirtualTourRoom])
    func reload(rooms: [VirtualTourRoom])
    func dismiss()
    
}

//
//  AddRoomsRouter.swift
//  VIPER best practices
//
//  Created by Tibor Bödecs on 2019. 03. 05..
//

import Foundation

protocol AddRoomRouter {
    
    var presenter: AddRoomPresenter? { get set }
    
    func showAddRoomPage()
    func showDescription()
    func openViewer(index: Int)
}

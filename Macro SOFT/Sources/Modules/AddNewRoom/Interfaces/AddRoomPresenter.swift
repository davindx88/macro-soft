//
//  AddRoomsPresenter.swift
//  VIPER best practices
//
//  Created by Tibor Bödecs on 2019. 03. 05..
//

import Foundation

protocol AddRoomPresenter: class {
    
    var router: AddRoomRouter? { get set }
    var interactor: AddRoomInteractor? { get set }
    var view: AddRoomView? { get set }
    func viewDidLoad()
    func btnAddRoomTapped()
    func descriptionTapped()
    func roomTapped(index: Int)
    func reload()
    
}

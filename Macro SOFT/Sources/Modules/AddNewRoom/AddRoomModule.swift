//
//  AddRoomsDefaultBuilder.swift
//  VIPER best practices
//
//  Created by Tibor Bödecs on 2019. 03. 05..
//

import Foundation
import UIKit

class AddRoomModule {
    func buildDefault(roomPoint: RoomPoint, parentRoomId: String, onPointAdded:@escaping () -> Void) -> UIViewController {
        let view = AddRoomDefaultView()
        let interactor = AddRoomDefaultInteractor()
        let presenter = AddRoomDefaultPresenter(roomPoint: roomPoint, parentId: parentRoomId, onPointAdded: onPointAdded)
        let router = AddRoomDefaultRouter()

        view.presenter = presenter

        presenter.interactor = interactor
        presenter.view = view
        presenter.router = router
        
        interactor.presenter = presenter

        router.presenter = presenter
        router.viewController = view
        let navControllerView = UINavigationController()
        navControllerView.pushViewController(view, animated: false)
        
        return navControllerView
    }
    
}

//
//  AddRoomsDefaultRouter.swift
//  VIPER best practices
//
//  Created by Tibor Bödecs on 2019. 03. 05..
//

import Foundation
import UIKit

class AddRoomDefaultRouter {
    weak var presenter: AddRoomPresenter?
    weak var viewController: UIViewController?
}

extension AddRoomDefaultRouter: AddRoomRouter {
    func showAddRoomPage() {
        let viewController = Add360Module().buildDefault1(openFrom: .fromNode){
            
        }
        
        print("touch3")
        
        if let vc = viewController.viewControllers[0] as? Add360DefaultView {
            vc.onSuccesAddRoom = {
                self.presenter?.reload()
            }
        }
        
        self.viewController?.present(viewController, animated: true, completion:{
            print("24")
        })
    }
    
    func showDescription() {
        let viewController = DescriptionModule().buildDefault()
        self.viewController?.present(viewController, animated: true)
    }
    
    func openViewer(index: Int) {
//        let viewController = Viewer360Module().loadImage(index: index,jenisViewer: .add)
//        viewController.definesPresentationContext = true
//        viewController.modalPresentationStyle = .overCurrentContext
//        self.viewController?.navigationController?.present(viewController, animated: true)
    }
}



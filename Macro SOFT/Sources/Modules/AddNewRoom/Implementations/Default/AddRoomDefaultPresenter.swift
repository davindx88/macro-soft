//
//  AddRoomsDefaultPresenter.swift
//  VIPER best practices
//
//  Created by Tibor Bödecs on 2019. 03. 05..
//

import Foundation

class AddRoomDefaultPresenter {
    var router: AddRoomRouter?
    var interactor: AddRoomInteractor?
    weak var view: AddRoomView?
    
    var roomPoint: RoomPoint
    var parentId: String
    var onPointAdded: () -> Void
    
    init(roomPoint: RoomPoint, parentId: String, onPointAdded: @escaping()->Void = {}){
        self.roomPoint = roomPoint
        self.parentId = parentId
        self.onPointAdded = onPointAdded
    }
}

extension AddRoomDefaultPresenter: AddRoomPresenter {
    func viewDidLoad() {
        view?.setup()
        let room: [VirtualTourRoom] = App.shared.virtualTourService.getTempVirtualTour().rooms
        view?.displayRooms(rooms: room)
    }

    func btnAddRoomTapped() {
        print("Pindah ke Add Room")
        router?.showAddRoomPage()
    }
    
    func descriptionTapped(){
        router?.showDescription()
    }
    
    func roomTapped(index: Int){
        let x = roomPoint.nodeRoom.position.x
        let y = roomPoint.nodeRoom.position.y
        let z = roomPoint.nodeRoom.position.z
        let room: VirtualTourRoom = App.shared.virtualTourService.getRoomOfTempVirtualTour(roomIdx: index)
        
        let isEdit = roomPoint.point?.roomId != nil
        if isEdit{
            App.shared.virtualTourService.removePointOfTempVirtualTourRoom(roomId: parentId, pointId: roomPoint.point!.pointId)
            let point: VirtualTourRoomPoint = VirtualTourRoomPoint(roomId: room.id, x: x, y: y, z: z)
            App.shared.virtualTourService.addPointToTempVirtualTourRoom(roomId: parentId, point: point)
        }
        else {
            let point: VirtualTourRoomPoint = VirtualTourRoomPoint(roomId: room.id, x: x, y: y, z: z)
            App.shared.virtualTourService.addPointToTempVirtualTourRoom(roomId: parentId, point: point)
        }
        
        view?.dismiss()
        onPointAdded()
    }
    
    func reload(){
        let rooms: [VirtualTourRoom] = App.shared.virtualTourService.getRoomsOfTempVirtualTour()
        view?.reload(rooms: rooms)
    }
}

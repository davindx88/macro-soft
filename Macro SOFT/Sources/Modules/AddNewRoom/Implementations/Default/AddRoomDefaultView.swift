//
//  AddRoomDefaultView.swift
//  Macro SOFT
//
//  Created by rubby handojo on 26/10/21.
//

import UIKit

class AddRoomDefaultView: UIViewController,UITableViewDelegate, UITableViewDataSource {
    var presenter: AddRoomPresenter?
    var rooms: [VirtualTourRoom] = []
    var selectedIdx: Int = -1
    @IBOutlet weak var roomTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return rooms.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let roomcell = tableView.dequeueReusableCell(withIdentifier: "Room",for: indexPath) as! AddNewRoomViewCell
        let btnCell = tableView.dequeueReusableCell(withIdentifier: "ButtonAddRoom", for: indexPath) as! ButtonAddViewCell
        btnCell.presenter = self.presenter
        
        if  (isCreateNamaRow(idxRow: indexPath.section)){
            return btnCell
        } else {
            let room = rooms[indexPath.section]
            let viewModel = RoomViewModel(room: room)
            roomcell.RoomModel = viewModel
            return roomcell
        }
    }
    
//    func check (index: Int,namaRuangan:String,fotoRuangan:UIImage){
//        var check = self.rooms[index]
//        check.check = true
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //let room = rooms[indexPath.section]
        //let image = room.fotoRuangan
        if (indexPath.section <= rooms.count) {
            presenter?.roomTapped(index: indexPath.section)
            
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AddRoomDefaultView:AddRoomView{
    func isCreateNamaRow(idxRow: Int) -> Bool {
        idxRow == rooms.count
    }
    
    func setup(){
        self.title = "Add Room"
   
        
        self.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        let buttonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(self.backPressed))
      
        self.navigationItem.setLeftBarButton(buttonItem, animated: false)
    }
    
    @objc func backPressed(_ sender: Any){
        self.dismiss(animated: true)
    }
    
    func displayRooms(rooms: [VirtualTourRoom]){
        self.rooms = rooms
        roomTableView.showsVerticalScrollIndicator = false
        roomTableView.register(UINib(nibName: "AddNewRoomViewCell", bundle: nil), forCellReuseIdentifier: "Room")
        roomTableView.register(UINib(nibName: "ButtonAddViewCell", bundle: nil), forCellReuseIdentifier: "ButtonAddRoom")
        roomTableView.dataSource = self
        roomTableView.delegate = self
        roomTableView.reloadData()
    }
    
    func reload(rooms: [VirtualTourRoom]){
        self.rooms = rooms
        roomTableView.reloadData()
    }
    
    func dismiss() {
        self.dismiss(animated: true)
    }
}

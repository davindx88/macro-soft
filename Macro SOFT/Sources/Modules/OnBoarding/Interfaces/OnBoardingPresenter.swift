//
//  OnBoardingPresenter.swift
//  VIPER best practices
//
//  Created by Tibor Bödecs on 2019. 03. 05..
//

import Foundation

protocol OnBoardingPresenter: class {
    
    var router: OnBoardingRouter? { get set }
    var interactor: OnBoardingInteractor? { get set }
    var view: OnBoardingView? { get set }
    
    func viewDidLoad()
    func openProperty()
}

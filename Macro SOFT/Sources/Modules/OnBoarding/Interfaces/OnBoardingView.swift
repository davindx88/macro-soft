//
//  OnBoardingView.swift
//  VIPER best practices
//
//  Created by Tibor Bödecs on 2019. 03. 05..
//

import Foundation

protocol OnBoardingView: class {
    
    var presenter: OnBoardingPresenter? { get set }
   
    func displayOnboarding()
    
    
}

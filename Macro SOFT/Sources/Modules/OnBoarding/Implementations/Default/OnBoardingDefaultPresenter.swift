//
//  OnBoardingDefaultPresenter.swift
//  VIPER best practices
//
//  Created by Tibor Bödecs on 2019. 03. 05..
//

import Foundation

class OnBoardingDefaultPresenter {
    
    var router: OnBoardingRouter?
    var interactor: OnBoardingInteractor?
    weak var view: OnBoardingView?
}

extension OnBoardingDefaultPresenter: OnBoardingPresenter {
    func openProperty() {
        router?.showproperty()
    }
    

    func viewDidLoad() {
        view?.displayOnboarding()
      
    }

    
}

//
//  OnBoardingDefaultView.swift
//  Macro SOFT
//
//  Created by rubby handojo on 09/11/21.
//

import UIKit

class OnBoardingDefaultView: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
   
    var presenter: OnBoardingPresenter?
    @IBOutlet var btnNext: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var onboardingCollectionView: UICollectionView!
    var slides: [InfoOnBoarding] = []
    var currentPage = 0 {
        didSet{
            pageControl.currentPage = currentPage
            if currentPage == slides.count-1 {
                btnNext.setTitle("Let's Go Virtual! ", for: .normal)
            }
            else {
                btnNext.setTitle("Selanjutnya ->", for: .normal)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        slides = [InfoOnBoarding(title: "Tambahkan Ruangan 360° Anda", subTitle: "Tambahkan foto ruangan 360° anda ke dalam aplikasi Houscene", image: UIImage(named: "Onboarding1")!),
                  InfoOnBoarding(title: "Letakkan Point untuk Ruangan Selanjutnya", subTitle: "Letakkan point pada tempat yang di inginkan untuk menuju ke ruangan berikutnya", image: UIImage(named: "Onboarding2")!),
                  InfoOnBoarding(title: "Tambahkan Deskripsi Rumah Anda", subTitle: "Tambahkan deskripsi dari virtual tour rumah yang telah anda buatTambahkan deskripsi dari virtual tour rumah yang telah anda buat", image: UIImage(named: "Onboarding3")!)
        ]
        onboardingCollectionView.delegate = self
        onboardingCollectionView.dataSource = self

        self.presenter?.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return slides.count
    }
    
    @IBAction func btnNextPressed(_ sender: Any) {
        
        if currentPage == slides.count-1{
            App.shared.infoOnboarding.isNotNewUser()
            presenter?.openProperty()
            
        }else{
            App.shared.infoOnboarding.isNewUser()
            currentPage += 1
            let index = IndexPath(item: currentPage, section: 0)
            onboardingCollectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = onboardingCollectionView.dequeueReusableCell(withReuseIdentifier: "OnBoarding", for: indexPath)as! OnBoardingCollectionViewCell
//        cell.setup(slides[indexPath.row])
        cell.presenter = self.presenter
        let onboarding = slides[indexPath.row]
        let viewModel = OnBoardingViewModel(onboarding: onboarding)
        cell.OnboardingModel = viewModel
       
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: onboardingCollectionView.frame.width, height: onboardingCollectionView.frame.height)
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let width = scrollView.frame.width
        currentPage = Int(scrollView.contentOffset.x/width)
        
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension OnBoardingDefaultView: OnBoardingView{
  
    
    
    func displayOnboarding(){
        onboardingCollectionView.register(UINib(nibName: "OnBoardingCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "OnBoarding")
    
    }
    
    
}

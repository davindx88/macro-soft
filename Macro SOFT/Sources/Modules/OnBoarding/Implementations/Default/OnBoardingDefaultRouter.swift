//
//  OnBoardingDefaultRouter.swift
//  VIPER best practices
//
//  Created by Tibor Bödecs on 2019. 03. 05..
//

import Foundation
import UIKit

class OnBoardingDefaultRouter {

    weak var presenter: OnBoardingPresenter?
    weak var viewController: UIViewController?
}

extension OnBoardingDefaultRouter: OnBoardingRouter {
    func showproperty() {
        let viewController = PropertiesModule().buildDefault()
        viewController.modalPresentationStyle = .fullScreen
        self.viewController?.present(viewController, animated: true, completion: nil)
    }
    

  
}



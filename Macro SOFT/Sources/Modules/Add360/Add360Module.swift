//
//  Add360Module.swift
//  Macro SOFT
//
//  Created by Vincentius Phillips Zhuputra on 14/10/21.
//

import Foundation
import UIKit

class Add360Module {
    func buildDefault1(openFrom: OpenFrom,onClose:@escaping ()->Void) -> UINavigationController {
        let view = Add360DefaultView()
        let interactor = Add360DefaultInteractor()
        let presenter = Add360DefaultPresenter()
        let router = Add360DefaultRouter()

        view.presenter = presenter
        
        presenter.interactor = interactor
        presenter.view = view
        presenter.router = router
        presenter.openFrom = openFrom
        presenter.onClose = onClose
        interactor.presenter = presenter
        
        router.presenter = presenter
        router.viewController = view
        let navController = UINavigationController()

        navController.pushViewController(view, animated: false)
        return navController
    }
}


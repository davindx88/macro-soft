//
//  Add360DefaultRouter.swift
//  Macro SOFT
//
//  Created by Vincentius Phillips Zhuputra on 14/10/21.
//

import Foundation
import UIKit

class Add360DefaultRouter {

    weak var presenter: Add360Presenter?
    weak var viewController: UIViewController?
}

extension Add360DefaultRouter: Add360Router {
    func openViewer(roomId: String) {
        let viewController = Viewer360Module().loadImage(roomId: roomId, jenisViewer: .add){
            self.presenter?.onAddRoom()
        }
        //        self.viewController?.present(viewController, animated: true)

        //        viewController.modalPresentationStyle = .fullScreen
        self.viewController?.navigationController?.pushViewController(viewController, animated: true)
    }
}

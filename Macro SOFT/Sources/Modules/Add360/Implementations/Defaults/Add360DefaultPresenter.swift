//
//  Add360DefaultPresenter.swift
//  Macro SOFT
//
//  Created by Vincentius Phillips Zhuputra on 14/10/21.
//

import Foundation
import UIKit
enum OpenFrom{
    case fromNode
    case fromModalList
}

class Add360DefaultPresenter {
    var router: Add360Router?
    var interactor: Add360Interactor?
    weak var view: Add360View?
    var openFrom: OpenFrom?
    var onClose: (()->Void)? = {}
    var _listNama: [String] =
    [
        "Ruang Tidur",
        "Ruang Makan",
        "Ruang Keluarga",
        "Toilet/Kamar Mandi",
        "Garasi",
        "Teras",
        "Dapur"
    ]
}

extension Add360DefaultPresenter: Add360Presenter {
    func viewDidLoad() {
        view?.setup()
        let namaRoom: [NamaRoom] = App.shared.listNamaRoom.listNamaRoom()
        view?.displayListNama(_listNama: namaRoom)
    }
    
    func galImgPicked(namaRuangan: String, fotoRuangan: UIImage) {
        if openFrom == .fromModalList{
            let newVTRoom: VirtualTourRoom = VirtualTourRoom(name: namaRuangan, uiImage: fotoRuangan)
            App.shared.virtualTourService.addRoomToTempVirtualTour(room: newVTRoom)
            router?.openViewer(roomId: newVTRoom.id)
        }
        else if openFrom == .fromNode{
            var newVTRoom: VirtualTourRoom = VirtualTourRoom(name: namaRuangan, uiImage: fotoRuangan)
            newVTRoom.isTemporary = false
            App.shared.virtualTourService.addRoomToTempVirtualTour(room: newVTRoom)
            view?.closeModal()
        }
    }
    
    func addNamaRoom(nama: String) {
        print("40")
        view?.addNamaRoom(nama: nama)
    }
    
    func onAddRoom() {
        print("testing")
        self.onClose!()
    }
//    func cellSelect(idx: Int) -> Int{
////        let oldIdx = selectedIdx
////        let newIdx = idx
////        selectedIdx = newIdx
////        // Update Display
////        var updatedIdx = [Int]()
////        updatedIdx.append(newIdx)
////        if oldIdx >= 0 { //Jika old idx valid, maka udpate juga old idx
////            updatedIdx.append(oldIdx)
////        }
////        return selectedIdx
//    }
}


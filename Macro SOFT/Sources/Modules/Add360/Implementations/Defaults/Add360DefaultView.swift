//
//  Add360DefaultView.swift
//  Macro SOFT
//
//  Created by Vincentius Phillips Zhuputra on 14/10/21.
//

import UIKit

class Add360DefaultView: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {

    var presenter: Add360Presenter?
    var listNama: [NamaRoom] = []
    var namaRoom = ""
    var onSuccesAddRoom: () -> Void = {}
    @IBOutlet weak var tblViewNamaRoom: UITableView!
    @IBOutlet weak var imgViewInsert360: UIImageView!
    @IBOutlet weak var btnOpenGal: UIButton!
    
//    var listNama: [String] =
//    [
//        "Ruang Tidur",
//        "Ruang Makan",
//        "Ruang Keluarga",
//        "Toilet/Kamar Mandi",
//        "Garasi",
//        "Teras",
//        "Dapur"
//    ]
    var selectedIdx: Int = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listNama.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Add360", for: indexPath) as! Add360ViewCell
        let cellAdd = tableView.dequeueReusableCell(withIdentifier: "AddRoom", for: indexPath) as! AddRoomViewCell
        if (isCreateNamaRow(idxRow: indexPath.row)) {
            cellAdd.presenter = presenter
            return cellAdd
        } else {
            cell.textLabel?.text = listNama[indexPath.row].namaRuangan
            if (selectedIdx == indexPath.row) {
                cell.imageView?.image = UIImage.checkmark
            } else {
                cell.imageView?.image = nil
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isCreateNamaRow(idxRow: indexPath.row){ // Jika Cell Create Voucher
            //editable row
        }else{
            //if selected
            namaRoom = App.shared.listNamaRoom.ListNamaRoom[indexPath.row].namaRuangan
            selectedIdx = indexPath.row
            tableView.reloadData()
        }
    }
    
    @IBAction func actionOpenGal(_ sender: Any) {
        let image = UIImagePickerController()
        image.delegate = self
        image.sourceType = UIImagePickerController.SourceType.photoLibrary
        image.allowsEditing = false
        self.present(image, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            picker.dismiss(animated: true, completion: nil)
            presenter?.galImgPicked(namaRuangan:namaRoom,fotoRuangan:image)
        }
    }
    
    
    
}

extension Add360DefaultView: Add360View {
    func isCreateNamaRow(idxRow: Int) -> Bool {
        idxRow == listNama.count
    }
    
    func setup(){
    self.title = "Add Room"
    
    let buttonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(self.backPressed))

    self.navigationItem.setLeftBarButton(buttonItem, animated: false)

    
    }
    @objc func backPressed(_ sender: Any){
        self.dismiss(animated: true)
     
    }
    func closeModal(){
        self.onSuccesAddRoom()
        self.dismiss(animated: true)
    }
    func displayListNama(_listNama: [NamaRoom]) {
        print("Property Loaded")
        self.listNama = _listNama
        tblViewNamaRoom.showsVerticalScrollIndicator = false
        tblViewNamaRoom.register(UINib(nibName: "Add360ViewCell", bundle: nil), forCellReuseIdentifier: "Add360")
        tblViewNamaRoom.register(UINib(nibName: "AddRoomViewCell", bundle: nil), forCellReuseIdentifier: "AddRoom")
        tblViewNamaRoom.dataSource = self
        tblViewNamaRoom.delegate = self
        tblViewNamaRoom.reloadData()
    }
    
    func addNamaRoom(nama: String) {
        let n =  NamaRoom(namaRuangan: nama)
        //listNama.append(n)
        App.shared.listNamaRoom.addNama(nama: n)
        let listNama = App.shared.listNamaRoom.listNamaRoom()
        self.listNama = listNama
        tblViewNamaRoom.reloadData()
    }
}

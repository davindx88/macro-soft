//
//  Add360Presenter.swift
//  Macro SOFT
//
//  Created by Vincentius Phillips Zhuputra on 14/10/21.
//

import Foundation
import UIKit

protocol Add360Presenter: class {
    
    var router: Add360Router? { get set }
    var interactor: Add360Interactor? { get set }
    var view: Add360View? { get set }
    
    func viewDidLoad()
    func galImgPicked(namaRuangan:String,fotoRuangan:UIImage)
    func addNamaRoom(nama: String)
    func onAddRoom()
    
    //func cellSelect(idx: Int) -> Int
}

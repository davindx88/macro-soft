//
//  Add360Router.swift
//  Macro SOFT
//
//  Created by Vincentius Phillips Zhuputra on 14/10/21.
//

import Foundation
import UIKit

protocol Add360Router {
    var presenter: Add360Presenter? { get set }
    
    func openViewer(roomId: String)
}


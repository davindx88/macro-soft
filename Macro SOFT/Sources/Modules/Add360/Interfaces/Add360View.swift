//
//  Add360View.swift
//  Macro SOFT
//
//  Created by Vincentius Phillips Zhuputra on 14/10/21.
//

import Foundation

protocol Add360View: class {
    
    var presenter: Add360Presenter? { get set }
    
    func setup()
    func displayListNama(_listNama: [NamaRoom])
    
    // MARK: Helper Function
    func isCreateNamaRow(idxRow: Int)->Bool
    func addNamaRoom(nama: String)
    func closeModal()
}

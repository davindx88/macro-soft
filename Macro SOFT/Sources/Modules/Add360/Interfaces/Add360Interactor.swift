//
//  Add360Interactor.swift
//  Macro SOFT
//
//  Created by Vincentius Phillips Zhuputra on 14/10/21.
//

import Foundation

protocol Add360Interactor {
    
    var presenter: Add360Presenter? { get set }
    
}

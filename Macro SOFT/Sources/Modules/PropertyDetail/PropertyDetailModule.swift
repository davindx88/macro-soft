//
//  PropertyDetailModule.swift
//  Macro SOFT
//
//  Created by Davin Djayadi on 12/10/21.
//

import Foundation
import UIKit

class PropertyDetailModule {
    func buildDefault(virtualTour: VirtualTour, onSuccessUpdate: @escaping()->Void ) -> UIViewController {
        let view = PropertyDetailDefaultView.loadFromNib()
        let interactor = PropertyDetailDefaultInteractor()
        let presenter = PropertyDetailDefaultPresenter(virtualTour: virtualTour, onSuccessUpdate: onSuccessUpdate)
        let router = PropertyDetailDefaultRouter()

        view.presenter = presenter

        presenter.interactor = interactor
        presenter.view = view
        presenter.router = router

        interactor.presenter = presenter

        router.presenter = presenter
        router.viewController = view
        
        return view
    }
}

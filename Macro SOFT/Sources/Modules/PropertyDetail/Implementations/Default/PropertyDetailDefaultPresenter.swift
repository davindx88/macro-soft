//
//  PropertyDetailDefaultPresenter.swift
//  Macro SOFT
//
//  Created by Davin Djayadi on 12/10/21.
//

import Foundation
import UIKit

class PropertyDetailDefaultPresenter {
    
    var router: PropertyDetailRouter?
    var interactor: PropertyDetailInteractor?
    weak var view: PropertyDetailView?
    
    let virtualTour: VirtualTour
    let onSuccessUpdate: ()->Void
    
    init(virtualTour: VirtualTour, onSuccessUpdate: @escaping()->Void){
        self.virtualTour = virtualTour
        self.onSuccessUpdate = onSuccessUpdate
    }
}

extension PropertyDetailDefaultPresenter: PropertyDetailPresenter{
    
    func viewDidLoad() {
        let viewModel = PropertyViewModel(virtualTour: self.virtualTour)
        view?.display(propertyViewModel: viewModel)
    }
    
    func view360Tap() {
        //App.shared.virtualTourService.setTempVirtualTour(virtualTour: virtualTour)
        App.shared.virtualTourService.loadRoomImagesOfTempVirtualTourRoom(
            onSuccess: { [weak self] in
                let room = App.shared.virtualTourService.getTempVirtualTour().rooms[0]
                self?.router?.openViewer(roomId: room.id)
            },
            onFailed: {
                print("Image not loaded")
            })
    }
    
    func edit360Tap() {
        // TODO edit
        App.shared.virtualTourService.loadRoomImagesOfTempVirtualTourRoom(
            onSuccess: { [weak self] in
                self?.router?.openEditor()
            },
            onFailed: {
                print("Image not loaded")
            })
    }
    
    func onVTUpdated() {
        print("VT Updated")
        self.onSuccessUpdate()
    }
}

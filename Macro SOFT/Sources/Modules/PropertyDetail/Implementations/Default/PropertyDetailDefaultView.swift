//
//  PropertyDetailDefaultView.swift
//  Macro SOFT
//
//  Created by Davin Djayadi on 12/10/21.
//

import UIKit

class PropertyDetailDefaultView: UIViewController {
    var presenter: PropertyDetailPresenter?
    
    @IBOutlet var propImageView: UIImageView!
    @IBOutlet var propNameLabel: UILabel!
    @IBOutlet var propAddrLabel: UILabel!
    @IBOutlet var propPriceLabel: UILabel!
    @IBOutlet var propAreaLabel: UILabel!
    @IBOutlet var totalBathroomLabel: UILabel!
    @IBOutlet var totalBedroomLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var view360view: UIView!
    
    @IBOutlet var editBtn: UIImageView!
    @IBOutlet var pencilView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupEditBtn()
        self.setupView360Btn()
        self.presenter?.viewDidLoad()
    }
    
    func setupEditBtn(){
        pencilView.layer.cornerRadius = 20
        
        let tapViewEdit360 = UITapGestureRecognizer(target: self, action: #selector(handleTapEdit(_:)))
        pencilView.addGestureRecognizer(tapViewEdit360)
    }
    
    @objc func handleTapEdit(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        print("43")
        presenter?.edit360Tap()
    }
    
    
    func setupView360Btn(){
        view360view.layer.cornerRadius = 40
        view360view.clipsToBounds = true
        
        // BLUR Reference https://www.raywenderlich.com/16125723-uivisualeffectview-tutorial-getting-started
        let blurEffect = UIBlurEffect(style: .extraLight) // .extraLight or .dark
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.translatesAutoresizingMaskIntoConstraints = false
        view360view.insertSubview(blurEffectView, at: 0)
        NSLayoutConstraint.activate([
            blurEffectView.topAnchor.constraint(equalTo: view360view.topAnchor),
            blurEffectView.leadingAnchor.constraint(equalTo: view360view.leadingAnchor),
            blurEffectView.widthAnchor.constraint(equalTo: view360view.widthAnchor),
            blurEffectView.heightAnchor.constraint(equalTo: view360view.heightAnchor),
        ])
        
        let tapView360 = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        view360view.addGestureRecognizer(tapView360)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        presenter?.view360Tap()
    }
}

extension PropertyDetailDefaultView: PropertyDetailView{
    func display(propertyViewModel: PropertyViewModel) {
        // TODO tampilkan fieldArea & certificate
        
//        self.title = propertyViewModel.name
        propImageView.load(urlString: propertyViewModel.imageStrURL)
        propNameLabel.text = propertyViewModel.name
        propAddrLabel.text = propertyViewModel.address
        propPriceLabel.text = propertyViewModel.priceStr
        propAreaLabel.text = propertyViewModel.propertyAreaStr
        totalBathroomLabel.text = propertyViewModel.nBathroomStr
        totalBedroomLabel.text = propertyViewModel.nBedroomStr
        descriptionLabel.text = propertyViewModel.description
    }
}

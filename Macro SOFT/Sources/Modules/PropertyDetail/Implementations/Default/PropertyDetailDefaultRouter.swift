//
//  PropertyDetailDefaultRouter.swift
//  Macro SOFT
//
//  Created by Davin Djayadi on 12/10/21.
//

import Foundation
import UIKit

class PropertyDetailDefaultRouter {

    weak var presenter: PropertyDetailPresenter?
    weak var viewController: UIViewController?
}

extension PropertyDetailDefaultRouter: PropertyDetailRouter {
    func openViewer(roomId: String) {
        let viewController = Viewer360Module().loadImage(roomId: roomId, jenisViewer: .view){}
        self.viewController?.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func openEditor() {
        let viewController = ModalListModule().buildUpdate {
            self.viewController?.navigationController?.popViewController(animated: true)
            self.presenter?.onVTUpdated()
        }
        self.viewController?.navigationController?.pushViewController(viewController, animated: true)
    }

}

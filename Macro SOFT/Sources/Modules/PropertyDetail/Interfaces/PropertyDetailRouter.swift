//
//  PropertyDetailRouter.swift
//  Macro SOFT
//
//  Created by Davin Djayadi on 12/10/21.
//

import Foundation


protocol PropertyDetailRouter {
    
    var presenter: PropertyDetailPresenter? { get set }
    
    func openViewer(roomId: String)
    func openEditor()
}

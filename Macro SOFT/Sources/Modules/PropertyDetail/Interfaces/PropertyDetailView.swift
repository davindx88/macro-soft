//
//  PropertyDetailView.swift
//  Macro SOFT
//
//  Created by Davin Djayadi on 12/10/21.
//

import Foundation

protocol PropertyDetailView: class {
    
    var presenter: PropertyDetailPresenter? { get set }
    
    func display(propertyViewModel: PropertyViewModel)
}

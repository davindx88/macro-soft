//
//  PropertyDetailInteractor.swift
//  Macro SOFT
//
//  Created by Davin Djayadi on 12/10/21.
//

import Foundation

protocol PropertyDetailInteractor {
    
    var presenter: PropertyDetailPresenter? { get set }
}


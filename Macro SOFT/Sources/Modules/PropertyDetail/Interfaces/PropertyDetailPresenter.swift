//
//  PropertyDetailPresenter.swift
//  Macro SOFT
//
//  Created by Davin Djayadi on 12/10/21.
//

import Foundation

protocol PropertyDetailPresenter: class {
    
    var router: PropertyDetailRouter? { get set }
    var interactor: PropertyDetailInteractor? { get set }
    var view: PropertyDetailView? { get set }
    
    func viewDidLoad()
    func view360Tap()
    func edit360Tap()
    func onVTUpdated()
}

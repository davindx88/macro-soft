//
//  HomeDefaultRouter.swift
//  VIPER best practices
//
//  Created by Tibor Bödecs on 2019. 03. 05..
//

import Foundation
import UIKit

class HomeDefaultRouter {

    weak var presenter: HomePresenter?
    weak var viewController: UIViewController?
    
}

extension HomeDefaultRouter: HomeRouter {

    func getViewControllers() -> [UIViewController] {
        let propertyView = PropertiesModule().buildDefault()
        propertyView.title = "Property"
        propertyView.tabBarItem.image = UIImage(systemName: "house.fill")
        
        return [
            propertyView
        ].map { UINavigationController(rootViewController: $0) }
    }
}
